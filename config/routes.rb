Myapp::Application.routes.draw do

  resources :topicos_conteudos

  resources :formato_conteudos

  resources :canais do
    collection do
      match "ver_conteudos_canal", via: [:get, :post]
    end
  end

  resources :salvar_tags do
    collection do
      match "salva_tag", via: [:get, :post]
    end
  end

  resources :pre_login do
    collection do
      match "index", via: [:get, :post]
    end
  end
  resources :meus_cursos do
    collection do
      match "visualizar_curso", via: [:get, :post]
    end
  end
  resources :carrinhos do
    collection do
      match "pagamento", via: [:get, :post]
      match "add_carrinho", via: [:get, :post]
      match "deleta_item", via: [:get, :post] 
      match "finaliza_compra", via: [:get, :post]
    end
  end
  resources :minha_lista do
    collection do
      match "add_lista", via: [:get, :post]
      match "deleta_lista", via: [:get, :post]
    end
  end

  resources :autor_conteudo_conteudos
  resources :conteudos
  resources :autor_conteudos
  resources :conteudo_tipos
  resources :empresarial_ramos
  resources :orientacoes_empresas
  resources :orientacoes_desafios
  resources :desafios_empresas do
    collection do
      match 'todos_desafios', via: [:get, :post]
    end
  end
  resources :metodologia_desafios
  resources :status_desafios
  resources :dilemas
  resources :areas_conhecimentos
  resources :empresas_pessoas
  resources :empresas
  resources :modulos_pessoas do
    collection do
      match 'escolher_modulo', via: [:get, :post]
      match 'setar_modulo', via: [:get, :post]
    end
  end
  resources :modulos
  resources :pessoas
  devise_for :usuarios
  # You can have the root of your site routed with "root"
  root to: 'pages#index'

  # All routes
  get "dashboards/dashboard_1"
  get "dashboards/dashboard_2"
  get "dashboards/dashboard_3"
  get "dashboards/dashboard_4"
  get "dashboards/dashboard_4_1"
  get "dashboards/dashboard_5"
  get "dashboards/home"

  get "layoutsoptions/index"
  get "layoutsoptions/off_canvas"

  get "graphs/flot"
  get "graphs/morris"
  get "graphs/rickshaw"
  get "graphs/chartjs"
  get "graphs/chartist"
  get "graphs/peity"
  get "graphs/sparkline"
  get "graphs/c3charts"

  get "mailbox/inbox"
  get "mailbox/email_view"
  get "mailbox/compose_email"
  get "mailbox/email_templates"
  get "mailbox/basic_action_email"
  get "mailbox/alert_email"
  get "mailbox/billing_email"

  get "metrics/index"

  get "widgets/index"

  get "forms/basic_forms"
  get "forms/advanced"
  get "forms/wizard"
  get "forms/file_upload"
  get "forms/text_editor"
  get "forms/markdown"

  get "appviews/contacts"
  get "appviews/profile"
  get "appviews/profile_two"
  get "appviews/contacts_two"
  get "appviews/projects"
  get "appviews/project_detail"
  get "appviews/file_menager"
  get "appviews/vote_list"
  get "appviews/calendar"
  get "appviews/faq"
  get "appviews/timeline"
  get "appviews/pin_board"
  get "appviews/teams_board"
  get "appviews/social_feed"
  get "appviews/clients"
  get "appviews/outlook_view"
  get "appviews/blog"
  get "appviews/article"
  get "appviews/issue_tracker"

  get "pages/search_results"
  get "pages/lockscreen"
  get "pages/invoice"
  get "pages/invoice_print"
  get "pages/login"
  get "pages/login_2"
  get "pages/forgot_password"
  get "pages/register"
  get "pages/not_found_error"
  get "pages/internal_server_error"
  get "pages/empty_page"
  get "pages/escolher_empresa"
  get "pages/escolher_modulo"
  get "pages/escolher_funcao"
  get "pages/index"
  get "pages/visualizar_curso"
  get "pages/landing_gerador"
  get "pages/painel_gerador"
  match "pages/assistir_curso", via: [:get, :post]
  match "pages/curso", via: [:get, :post]
  match "pages/modulo_gerador", via: [:get, :post]
  match "pages/painel", via: [:get, :post]
  match "pages/conteudos_canal", via: [:get, :post]
  match "pages/topicos_conteudo", via: [:get, :post]
  match "pages/topico_conteudo", via: [:get, :post]
  match "pages/show_topico", via: [:get, :post]
  match "pages/show_topicos", via: [:get, :post]
  match "pages/busca_conteudo", via: [:get, :post]
  match "pages/conteudo_busca", via: [:get, :post]
  match "pages/conteudo_error", via: [:get, :post]

  get "miscellaneous/notification"
  get "miscellaneous/nestablelist"
  get "miscellaneous/timeline_second_version"
  get "miscellaneous/forum_view"
  get "miscellaneous/forum_post_view"
  get "miscellaneous/google_maps"
  get "miscellaneous/code_editor"
  get "miscellaneous/modal_window"
  get "miscellaneous/validation"
  get "miscellaneous/tree_view"
  get "miscellaneous/chat_view"
  get "miscellaneous/agile_board"
  get "miscellaneous/diff"
  get "miscellaneous/sweet_alert"
  get "miscellaneous/idle_timer"
  get "miscellaneous/spinners"
  get "miscellaneous/live_favicon"
  get "miscellaneous/masonry"
  get "miscellaneous/tour"
  get "miscellaneous/loading_buttons"
  get "miscellaneous/clipboard"
  get "miscellaneous/truncate"
  get "miscellaneous/i18support"

  get "uielements/typography"
  get "uielements/icons"
  get "uielements/draggable_panels"
  get "uielements/resizeable_panels"
  get "uielements/buttons"
  get "uielements/video"
  get "uielements/tables_panels"
  get "uielements/tabs"
  get "uielements/notifications_tooltips"
  get "uielements/badges_labels_progress"

  get "gridoptions/index"

  get "tables/static_tables"
  get "tables/data_tables"
  get "tables/foo_tables"
  get "tables/jqgrid"

  get "commerce/products_grid"
  get "commerce/products_list"
  get "commerce/product_edit"
  get "commerce/product_detail"
  get "commerce/orders"
  get "commerce/cart"
  get "commerce/payments"

  get "gallery/basic_gallery"
  get "gallery/slick_carusela"
  get "gallery/bootstrap_carusela"

  get "cssanimations/index"

  get "landing/index"

end
