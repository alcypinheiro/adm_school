class AddConteudoIdToTopicosConteudos < ActiveRecord::Migration
  def change
    add_reference :topicos_conteudos, :conteudo, index: true, foreign_key: true
  end
end
