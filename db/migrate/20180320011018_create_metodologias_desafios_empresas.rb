class CreateMetodologiasDesafiosEmpresas < ActiveRecord::Migration
  def change
    create_table :metodologias_desafios_empresas do |t|
      t.references :desafios_empresa, index: true, foreign_key: true
      t.references :metodologia_desafio, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
