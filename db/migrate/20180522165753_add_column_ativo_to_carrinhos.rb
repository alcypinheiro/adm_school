class AddColumnAtivoToCarrinhos < ActiveRecord::Migration
  def change
    add_column :carrinhos, :ativo, :boolean, default: true
  end
end
