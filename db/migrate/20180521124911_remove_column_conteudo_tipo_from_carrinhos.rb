class RemoveColumnConteudoTipoFromCarrinhos < ActiveRecord::Migration
  def change
    remove_reference :carrinhos, :conteudo_tipo, index: true, foreign_key: true
  end
end
