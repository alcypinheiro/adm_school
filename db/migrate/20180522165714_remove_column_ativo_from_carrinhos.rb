class RemoveColumnAtivoFromCarrinhos < ActiveRecord::Migration
  def change
    remove_column :carrinhos, :ativo, :boolean
  end
end
