class CreateEmpresarialRamos < ActiveRecord::Migration
  def change
    create_table :empresarial_ramos do |t|
      t.string :nome
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
