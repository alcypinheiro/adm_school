class AddColumnPessoasToCarrinho < ActiveRecord::Migration
  def change
    add_reference :carrinhos, :pessoa, index: true, foreign_key: true
  end
end
