class CreateMeusCursos < ActiveRecord::Migration
  def change
    create_table :meus_cursos do |t|
      t.references :pessoa, index: true, foreign_key: true
      t.references :conteudo, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
