class AddColumnInstituicaoEnsinoToEmpresa < ActiveRecord::Migration
  def change
  	add_column :empresas, :instituicao_ensino, :boolean, default: false
  end
end
