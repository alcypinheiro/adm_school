class RemoveCamposFromCarrinhos < ActiveRecord::Migration
  def change
    remove_column :carrinhos, :nome, :string
    remove_reference :carrinhos, :autor_conteudo_conteudo, index: true, foreign_key: true
    remove_reference :carrinhos, :pessoa, index: true, foreign_key: true
    remove_column :carrinhos, :preco_sugerido, :decimal
  end
end
