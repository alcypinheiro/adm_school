class CreateDilemasDesafios < ActiveRecord::Migration
  def change
    create_table :dilemas_desafios do |t|
      t.references :desafios_empresa, index: true, foreign_key: true
      t.references :dilema, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
