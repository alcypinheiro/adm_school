class RemoveAutorConteudoConteudoFromItensCarrinhos < ActiveRecord::Migration
  def change
    remove_reference :itens_carrinhos, :autor_conteudo_conteudo, index: true, foreign_key: true
  end
end
