class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nome
      t.string :email
      t.string :telefone
      t.string :cnpj
      t.string :cidade
      t.string :estado
      t.references :usuario, index: true, foreign_key: true
      t.string :bairro
      t.string :endereco
      t.string :complemento
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
