class CreateTopicosConteudos < ActiveRecord::Migration
  def change
    create_table :topicos_conteudos do |t|
      t.string :descricao
      t.string :objetivo
      t.string :resumo
      t.references :formato_conteudo, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
