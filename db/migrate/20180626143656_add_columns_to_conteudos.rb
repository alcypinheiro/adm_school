class AddColumnsToConteudos < ActiveRecord::Migration
  def change
    add_column :conteudos, :objetivo, :text
    add_column :conteudos, :pre_requisito, :string
  end
end
