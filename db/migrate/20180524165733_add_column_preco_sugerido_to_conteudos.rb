class AddColumnPrecoSugeridoToConteudos < ActiveRecord::Migration
  def change
    add_column :conteudos, :preco_sugerido, :decimal
  end
end
