class CreateAreasConhecimentos < ActiveRecord::Migration
  def change
    create_table :areas_conhecimentos do |t|
      t.string :nome
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
