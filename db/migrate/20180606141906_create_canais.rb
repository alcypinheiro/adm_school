class CreateCanais < ActiveRecord::Migration
  def change
    create_table :canais do |t|
      t.string :nome
      t.string :objetivo
      t.date :data_criacao
      t.string :criador
      t.references :empresa, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
