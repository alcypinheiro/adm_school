class AddIdsToSalvarTags < ActiveRecord::Migration
  def change
    add_reference :salvar_tags, :conteudo, index: true, foreign_key: true
    add_reference :salvar_tags, :topicos_conteudo, index: true, foreign_key: true
  end
end
