class AddModulosPessoasToAutorConteudos < ActiveRecord::Migration
  def change
    add_reference :autor_conteudos, :modulos_pessoa, index: true, foreign_key: true
  end
end
