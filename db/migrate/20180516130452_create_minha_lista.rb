class CreateMinhaLista < ActiveRecord::Migration
  def change
    create_table :minha_lista do |t|
      t.references :conteudo, index: true, foreign_key: true
      t.references :pessoa, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
