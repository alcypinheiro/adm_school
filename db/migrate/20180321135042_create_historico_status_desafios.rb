class CreateHistoricoStatusDesafios < ActiveRecord::Migration
  def change
    create_table :historico_status_desafios do |t|
      t.references :desafios_empresa, index: true, foreign_key: true
      t.references :status_desafio, index: true, foreign_key: true
      t.date :data_inicio
      t.date :data_termino
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
