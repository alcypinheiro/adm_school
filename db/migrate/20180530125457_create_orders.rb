class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :carrinho, index: true, foreign_key: true
      t.decimal :preco_sugerido
      t.references :pessoa, index: true, foreign_key: true
      t.string :reference
      t.string :status
      t.boolean :ativo

      t.timestamps null: false
    end
  end
end
