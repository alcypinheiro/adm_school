class AddAttachmentVideoToTopicosConteudos < ActiveRecord::Migration
  def self.up
    change_table :topicos_conteudos do |t|
      t.attachment :video
    end
  end

  def self.down
    remove_attachment :topicos_conteudos, :video
  end
end
