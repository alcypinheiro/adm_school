class CreateFormatoConteudos < ActiveRecord::Migration
  def change
    create_table :formato_conteudos do |t|
      t.string :nome
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
