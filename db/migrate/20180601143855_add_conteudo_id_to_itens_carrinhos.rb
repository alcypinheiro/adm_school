class AddConteudoIdToItensCarrinhos < ActiveRecord::Migration
  def change
    add_reference :itens_carrinhos, :conteudo, index: true, foreign_key: true
  end
end
