class AddColumnAutorConteudoConteudoToCarrinhos < ActiveRecord::Migration
  def change
    add_reference :carrinhos, :autor_conteudo_conteudo, index: true, foreign_key: true
  end
end
