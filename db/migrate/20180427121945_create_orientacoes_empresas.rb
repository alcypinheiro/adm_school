class CreateOrientacoesEmpresas < ActiveRecord::Migration
  def change
    create_table :orientacoes_empresas do |t|
      t.string :nome
      t.string :email
      t.text :texto

      t.timestamps null: false
    end
  end
end
