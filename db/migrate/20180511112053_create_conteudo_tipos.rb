class CreateConteudoTipos < ActiveRecord::Migration
  def change
    create_table :conteudo_tipos do |t|
      t.string :nome
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
