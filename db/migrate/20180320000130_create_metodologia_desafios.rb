class CreateMetodologiaDesafios < ActiveRecord::Migration
  def change
    create_table :metodologia_desafios do |t|
      t.string :nome
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
