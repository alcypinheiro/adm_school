class RemoveRequisitosFromConteudos < ActiveRecord::Migration
  def change
    remove_column :conteudos, :requisitos, :string
  end
end
