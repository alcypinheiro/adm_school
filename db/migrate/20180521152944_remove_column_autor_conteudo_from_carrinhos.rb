class RemoveColumnAutorConteudoFromCarrinhos < ActiveRecord::Migration
  def change
    remove_reference :carrinhos, :autor_conteudo, index: true, foreign_key: true
  end
end
