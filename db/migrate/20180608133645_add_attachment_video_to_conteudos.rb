class AddAttachmentVideoToConteudos < ActiveRecord::Migration
  def self.up
    change_table :conteudos do |t|
      t.attachment :video
    end
  end

  def self.down
    remove_attachment :conteudos, :video
  end
end
