class CreatePessoas < ActiveRecord::Migration
  def change
    create_table :pessoas do |t|
      t.string :nome
      t.string :email
      t.string :telefone
      t.string :cidade
      t.string :estado
      t.boolean :ativo, default: true
      t.references :usuario, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
