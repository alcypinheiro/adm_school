class AddPessoaIdToConteudos < ActiveRecord::Migration
  def change
    add_reference :conteudos, :pessoa, index: true, foreign_key: true
  end
end
