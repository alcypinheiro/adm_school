class RemoveConteudoIdFromCarrinhos < ActiveRecord::Migration
  def change
    remove_reference :carrinhos, :conteudo, index: true, foreign_key: true
  end
end
