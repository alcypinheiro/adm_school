class CreateCarrinhos < ActiveRecord::Migration
  def change
    create_table :carrinhos do |t|
      t.references :conteudo_tipo, index: true, foreign_key: true
      t.string :nome
      t.references :autor_conteudo, index: true, foreign_key: true
      t.string :preco_sugerido
      t.boolean :ativo

      t.timestamps null: false
    end
  end
end
