class AddCanalToConteudos < ActiveRecord::Migration
  def change
    add_reference :conteudos, :canai, index: true, foreign_key: true
  end
end
