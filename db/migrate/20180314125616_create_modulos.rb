class CreateModulos < ActiveRecord::Migration
  def change
    create_table :modulos do |t|
      t.string :nome
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
