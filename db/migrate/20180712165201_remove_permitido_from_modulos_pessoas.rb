class RemovePermitidoFromModulosPessoas < ActiveRecord::Migration
  def change
    remove_column :modulos_pessoas, :permitido, :boolean
  end
end
