class RemovePessoaIdFromItensCarrinhos < ActiveRecord::Migration
  def change
    remove_reference :itens_carrinhos, :pessoa, index: true, foreign_key: true
  end
end
