class AddColumnConteudoToCarrinhos < ActiveRecord::Migration
  def change
    add_reference :carrinhos, :conteudo, index: true, foreign_key: true
  end
end
