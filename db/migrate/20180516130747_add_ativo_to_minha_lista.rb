class AddAtivoToMinhaLista < ActiveRecord::Migration
  def change
    add_column :minha_lista, :ativo, :boolean, default: true
  end
end
