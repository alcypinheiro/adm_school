class CreateStatusDesafios < ActiveRecord::Migration
  def change
    create_table :status_desafios do |t|
      t.string :nome
      t.integer :sequencia
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
