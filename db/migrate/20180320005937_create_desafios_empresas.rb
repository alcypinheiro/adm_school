class CreateDesafiosEmpresas < ActiveRecord::Migration
  def change
    create_table :desafios_empresas do |t|
      t.boolean :ensino, default: false
      t.boolean :profissional, default: false
      t.boolean :solucao, default: false
      t.references :empresa, index: true, foreign_key: true
      t.references :areas_conhecimento, index: true, foreign_key: true
      t.string :contextualizacao
      t.string :principais_problemas
      t.string :dados_mercado
      t.boolean :termo_autorizacao, default: false
      t.references :status_desafio, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
