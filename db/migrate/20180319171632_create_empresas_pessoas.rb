class CreateEmpresasPessoas < ActiveRecord::Migration
  def change
    create_table :empresas_pessoas do |t|
      t.references :pessoa, index: true, foreign_key: true
      t.references :empresa, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
