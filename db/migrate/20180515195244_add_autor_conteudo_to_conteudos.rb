class AddAutorConteudoToConteudos < ActiveRecord::Migration
  def change
    add_reference :conteudos, :autor_conteudo, index: true, foreign_key: true
  end
end
