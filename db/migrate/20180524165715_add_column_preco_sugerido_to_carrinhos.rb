class AddColumnPrecoSugeridoToCarrinhos < ActiveRecord::Migration
  def change
    add_column :carrinhos, :preco_sugerido, :decimal
  end
end
