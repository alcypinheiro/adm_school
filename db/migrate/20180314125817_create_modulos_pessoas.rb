class CreateModulosPessoas < ActiveRecord::Migration
  def change
    create_table :modulos_pessoas do |t|
      t.references :pessoa, index: true, foreign_key: true
      t.references :modulo, index: true, foreign_key: true
      t.boolean :permitido, default: false
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
