class CreateAutorConteudoConteudos < ActiveRecord::Migration
  def change
    create_table :autor_conteudo_conteudos do |t|
      t.string :nome
      t.references :autor_conteudo, index: true, foreign_key: true
      t.references :conteudo, index: true, foreign_key: true
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
