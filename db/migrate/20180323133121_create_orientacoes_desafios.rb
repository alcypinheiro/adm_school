class CreateOrientacoesDesafios < ActiveRecord::Migration
  def change
    create_table :orientacoes_desafios do |t|
      t.references :desafios_empresa, index: true, foreign_key: true
      t.text :orientacao
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
