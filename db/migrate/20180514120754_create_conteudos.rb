class CreateConteudos < ActiveRecord::Migration
  def change
    create_table :conteudos do |t|
      t.references :conteudo_tipo, index: true, foreign_key: true
      t.string :nome
      t.string :autor_conteudo
      t.string :preco_sugerido
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
