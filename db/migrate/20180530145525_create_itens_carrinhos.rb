class CreateItensCarrinhos < ActiveRecord::Migration
  def change
    create_table :itens_carrinhos do |t|
      t.references :carrinho, index: true, foreign_key: true
      t.string :nome
      t.references :autor_conteudo_conteudo, index: true, foreign_key: true
      t.references :pessoa, index: true, foreign_key: true
      t.decimal :preco_sugerido
      t.boolean :ativo, default:true

      t.timestamps null: false
    end
  end
end
