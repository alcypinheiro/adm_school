class AddPessoaIdToCanais < ActiveRecord::Migration
  def change
    add_reference :canais, :pessoa, index: true, foreign_key: true
  end
end
