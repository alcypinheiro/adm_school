require 'test_helper'

class CanaisControllerTest < ActionController::TestCase
  setup do
    @canai = canais(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:canais)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create canai" do
    assert_difference('Canai.count') do
      post :create, canai: { ativo: @canai.ativo, criador: @canai.criador, data_criacao: @canai.data_criacao, empresa_id: @canai.empresa_id, nome: @canai.nome, objetivo: @canai.objetivo }
    end

    assert_redirected_to canai_path(assigns(:canai))
  end

  test "should show canai" do
    get :show, id: @canai
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @canai
    assert_response :success
  end

  test "should update canai" do
    patch :update, id: @canai, canai: { ativo: @canai.ativo, criador: @canai.criador, data_criacao: @canai.data_criacao, empresa_id: @canai.empresa_id, nome: @canai.nome, objetivo: @canai.objetivo }
    assert_redirected_to canai_path(assigns(:canai))
  end

  test "should destroy canai" do
    assert_difference('Canai.count', -1) do
      delete :destroy, id: @canai
    end

    assert_redirected_to canais_path
  end
end
