require 'test_helper'

class FormatoConteudosControllerTest < ActionController::TestCase
  setup do
    @formato_conteudo = formato_conteudos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:formato_conteudos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create formato_conteudo" do
    assert_difference('FormatoConteudo.count') do
      post :create, formato_conteudo: { ativo: @formato_conteudo.ativo, nome: @formato_conteudo.nome }
    end

    assert_redirected_to formato_conteudo_path(assigns(:formato_conteudo))
  end

  test "should show formato_conteudo" do
    get :show, id: @formato_conteudo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @formato_conteudo
    assert_response :success
  end

  test "should update formato_conteudo" do
    patch :update, id: @formato_conteudo, formato_conteudo: { ativo: @formato_conteudo.ativo, nome: @formato_conteudo.nome }
    assert_redirected_to formato_conteudo_path(assigns(:formato_conteudo))
  end

  test "should destroy formato_conteudo" do
    assert_difference('FormatoConteudo.count', -1) do
      delete :destroy, id: @formato_conteudo
    end

    assert_redirected_to formato_conteudos_path
  end
end
