require 'test_helper'

class MinhaListaControllerTest < ActionController::TestCase
  setup do
    @minha_listum = minha_lista(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:minha_lista)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create minha_listum" do
    assert_difference('MinhaListum.count') do
      post :create, minha_listum: { conteudo_id: @minha_listum.conteudo_id, pessoa_id: @minha_listum.pessoa_id }
    end

    assert_redirected_to minha_listum_path(assigns(:minha_listum))
  end

  test "should show minha_listum" do
    get :show, id: @minha_listum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @minha_listum
    assert_response :success
  end

  test "should update minha_listum" do
    patch :update, id: @minha_listum, minha_listum: { conteudo_id: @minha_listum.conteudo_id, pessoa_id: @minha_listum.pessoa_id }
    assert_redirected_to minha_listum_path(assigns(:minha_listum))
  end

  test "should destroy minha_listum" do
    assert_difference('MinhaListum.count', -1) do
      delete :destroy, id: @minha_listum
    end

    assert_redirected_to minha_lista_path
  end
end
