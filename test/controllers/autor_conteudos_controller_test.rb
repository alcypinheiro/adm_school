require 'test_helper'

class AutorConteudosControllerTest < ActionController::TestCase
  setup do
    @autor_conteudo = autor_conteudos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:autor_conteudos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create autor_conteudo" do
    assert_difference('AutorConteudo.count') do
      post :create, autor_conteudo: { ativo: @autor_conteudo.ativo, nome: @autor_conteudo.nome }
    end

    assert_redirected_to autor_conteudo_path(assigns(:autor_conteudo))
  end

  test "should show autor_conteudo" do
    get :show, id: @autor_conteudo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @autor_conteudo
    assert_response :success
  end

  test "should update autor_conteudo" do
    patch :update, id: @autor_conteudo, autor_conteudo: { ativo: @autor_conteudo.ativo, nome: @autor_conteudo.nome }
    assert_redirected_to autor_conteudo_path(assigns(:autor_conteudo))
  end

  test "should destroy autor_conteudo" do
    assert_difference('AutorConteudo.count', -1) do
      delete :destroy, id: @autor_conteudo
    end

    assert_redirected_to autor_conteudos_path
  end
end
