require 'test_helper'

class StatusDesafiosControllerTest < ActionController::TestCase
  setup do
    @status_desafio = status_desafios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:status_desafios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create status_desafio" do
    assert_difference('StatusDesafio.count') do
      post :create, status_desafio: { ativo: @status_desafio.ativo, nome: @status_desafio.nome, sequencia: @status_desafio.sequencia }
    end

    assert_redirected_to status_desafio_path(assigns(:status_desafio))
  end

  test "should show status_desafio" do
    get :show, id: @status_desafio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @status_desafio
    assert_response :success
  end

  test "should update status_desafio" do
    patch :update, id: @status_desafio, status_desafio: { ativo: @status_desafio.ativo, nome: @status_desafio.nome, sequencia: @status_desafio.sequencia }
    assert_redirected_to status_desafio_path(assigns(:status_desafio))
  end

  test "should destroy status_desafio" do
    assert_difference('StatusDesafio.count', -1) do
      delete :destroy, id: @status_desafio
    end

    assert_redirected_to status_desafios_path
  end
end
