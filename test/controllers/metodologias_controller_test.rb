require 'test_helper'

class MetodologiasControllerTest < ActionController::TestCase
  setup do
    @metodologias = metodologias(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:metodologias)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create metodologias" do
    assert_difference('Metodologias.count') do
      post :create, metodologias: { ativo: @metodologias.ativo, nome: @metodologias.nome }
    end

    assert_redirected_to metodologias_path(assigns(:metodologias))
  end

  test "should show metodologias" do
    get :show, id: @metodologias
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @metodologias
    assert_response :success
  end

  test "should update metodologias" do
    patch :update, id: @metodologias, metodologias: { ativo: @metodologias.ativo, nome: @metodologias.nome }
    assert_redirected_to metodologias_path(assigns(:metodologias))
  end

  test "should destroy metodologias" do
    assert_difference('Metodologias.count', -1) do
      delete :destroy, id: @metodologias
    end

    assert_redirected_to metodologias_index_path
  end
end
