require 'test_helper'

class DesafiosEmpresasControllerTest < ActionController::TestCase
  setup do
    @desafios_empresa = desafios_empresas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:desafios_empresas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create desafios_empresa" do
    assert_difference('DesafiosEmpresa.count') do
      post :create, desafios_empresa: { areas_conhecimento_id: @desafios_empresa.areas_conhecimento_id, ativo: @desafios_empresa.ativo, contextualizacao: @desafios_empresa.contextualizacao, dados_mercado: @desafios_empresa.dados_mercado, empresa_id: @desafios_empresa.empresa_id, ensino: @desafios_empresa.ensino, principais_problemas: @desafios_empresa.principais_problemas, profissional: @desafios_empresa.profissional, solucao: @desafios_empresa.solucao, termo_autorizacao: @desafios_empresa.termo_autorizacao }
    end

    assert_redirected_to desafios_empresa_path(assigns(:desafios_empresa))
  end

  test "should show desafios_empresa" do
    get :show, id: @desafios_empresa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @desafios_empresa
    assert_response :success
  end

  test "should update desafios_empresa" do
    patch :update, id: @desafios_empresa, desafios_empresa: { areas_conhecimento_id: @desafios_empresa.areas_conhecimento_id, ativo: @desafios_empresa.ativo, contextualizacao: @desafios_empresa.contextualizacao, dados_mercado: @desafios_empresa.dados_mercado, empresa_id: @desafios_empresa.empresa_id, ensino: @desafios_empresa.ensino, principais_problemas: @desafios_empresa.principais_problemas, profissional: @desafios_empresa.profissional, solucao: @desafios_empresa.solucao, termo_autorizacao: @desafios_empresa.termo_autorizacao }
    assert_redirected_to desafios_empresa_path(assigns(:desafios_empresa))
  end

  test "should destroy desafios_empresa" do
    assert_difference('DesafiosEmpresa.count', -1) do
      delete :destroy, id: @desafios_empresa
    end

    assert_redirected_to desafios_empresas_path
  end
end
