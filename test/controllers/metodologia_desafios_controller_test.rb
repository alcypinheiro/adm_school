require 'test_helper'

class MetodologiaDesafiosControllerTest < ActionController::TestCase
  setup do
    @metodologia_desafio = metodologia_desafios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:metodologia_desafios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create metodologia_desafio" do
    assert_difference('MetodologiaDesafio.count') do
      post :create, metodologia_desafio: { ativo: @metodologia_desafio.ativo, nome: @metodologia_desafio.nome }
    end

    assert_redirected_to metodologia_desafio_path(assigns(:metodologia_desafio))
  end

  test "should show metodologia_desafio" do
    get :show, id: @metodologia_desafio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @metodologia_desafio
    assert_response :success
  end

  test "should update metodologia_desafio" do
    patch :update, id: @metodologia_desafio, metodologia_desafio: { ativo: @metodologia_desafio.ativo, nome: @metodologia_desafio.nome }
    assert_redirected_to metodologia_desafio_path(assigns(:metodologia_desafio))
  end

  test "should destroy metodologia_desafio" do
    assert_difference('MetodologiaDesafio.count', -1) do
      delete :destroy, id: @metodologia_desafio
    end

    assert_redirected_to metodologia_desafios_path
  end
end
