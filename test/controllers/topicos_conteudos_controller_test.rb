require 'test_helper'

class TopicosConteudosControllerTest < ActionController::TestCase
  setup do
    @topicos_conteudo = topicos_conteudos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:topicos_conteudos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create topicos_conteudo" do
    assert_difference('TopicosConteudo.count') do
      post :create, topicos_conteudo: { ativo: @topicos_conteudo.ativo, descricao: @topicos_conteudo.descricao, formato_conteudo_id: @topicos_conteudo.formato_conteudo_id, objetivo: @topicos_conteudo.objetivo, resumo: @topicos_conteudo.resumo }
    end

    assert_redirected_to topicos_conteudo_path(assigns(:topicos_conteudo))
  end

  test "should show topicos_conteudo" do
    get :show, id: @topicos_conteudo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @topicos_conteudo
    assert_response :success
  end

  test "should update topicos_conteudo" do
    patch :update, id: @topicos_conteudo, topicos_conteudo: { ativo: @topicos_conteudo.ativo, descricao: @topicos_conteudo.descricao, formato_conteudo_id: @topicos_conteudo.formato_conteudo_id, objetivo: @topicos_conteudo.objetivo, resumo: @topicos_conteudo.resumo }
    assert_redirected_to topicos_conteudo_path(assigns(:topicos_conteudo))
  end

  test "should destroy topicos_conteudo" do
    assert_difference('TopicosConteudo.count', -1) do
      delete :destroy, id: @topicos_conteudo
    end

    assert_redirected_to topicos_conteudos_path
  end
end
