require 'test_helper'

class OrientacoesEmpresasControllerTest < ActionController::TestCase
  setup do
    @orientacoes_empresa = orientacoes_empresas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orientacoes_empresas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orientacoes_empresa" do
    assert_difference('OrientacoesEmpresa.count') do
      post :create, orientacoes_empresa: { email: @orientacoes_empresa.email, nome: @orientacoes_empresa.nome, texto: @orientacoes_empresa.texto }
    end

    assert_redirected_to orientacoes_empresa_path(assigns(:orientacoes_empresa))
  end

  test "should show orientacoes_empresa" do
    get :show, id: @orientacoes_empresa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orientacoes_empresa
    assert_response :success
  end

  test "should update orientacoes_empresa" do
    patch :update, id: @orientacoes_empresa, orientacoes_empresa: { email: @orientacoes_empresa.email, nome: @orientacoes_empresa.nome, texto: @orientacoes_empresa.texto }
    assert_redirected_to orientacoes_empresa_path(assigns(:orientacoes_empresa))
  end

  test "should destroy orientacoes_empresa" do
    assert_difference('OrientacoesEmpresa.count', -1) do
      delete :destroy, id: @orientacoes_empresa
    end

    assert_redirected_to orientacoes_empresas_path
  end
end
