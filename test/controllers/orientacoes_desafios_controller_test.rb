require 'test_helper'

class OrientacoesDesafiosControllerTest < ActionController::TestCase
  setup do
    @orientacoes_desafio = orientacoes_desafios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orientacoes_desafios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orientacoes_desafio" do
    assert_difference('OrientacoesDesafio.count') do
      post :create, orientacoes_desafio: { ativo: @orientacoes_desafio.ativo, desafios_empresa_id: @orientacoes_desafio.desafios_empresa_id, orientacao: @orientacoes_desafio.orientacao }
    end

    assert_redirected_to orientacoes_desafio_path(assigns(:orientacoes_desafio))
  end

  test "should show orientacoes_desafio" do
    get :show, id: @orientacoes_desafio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orientacoes_desafio
    assert_response :success
  end

  test "should update orientacoes_desafio" do
    patch :update, id: @orientacoes_desafio, orientacoes_desafio: { ativo: @orientacoes_desafio.ativo, desafios_empresa_id: @orientacoes_desafio.desafios_empresa_id, orientacao: @orientacoes_desafio.orientacao }
    assert_redirected_to orientacoes_desafio_path(assigns(:orientacoes_desafio))
  end

  test "should destroy orientacoes_desafio" do
    assert_difference('OrientacoesDesafio.count', -1) do
      delete :destroy, id: @orientacoes_desafio
    end

    assert_redirected_to orientacoes_desafios_path
  end
end
