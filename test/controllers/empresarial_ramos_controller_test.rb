require 'test_helper'

class EmpresarialRamosControllerTest < ActionController::TestCase
  setup do
    @empresarial_ramo = empresarial_ramos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:empresarial_ramos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create empresarial_ramo" do
    assert_difference('EmpresarialRamo.count') do
      post :create, empresarial_ramo: { ativo: @empresarial_ramo.ativo, nome: @empresarial_ramo.nome }
    end

    assert_redirected_to empresarial_ramo_path(assigns(:empresarial_ramo))
  end

  test "should show empresarial_ramo" do
    get :show, id: @empresarial_ramo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @empresarial_ramo
    assert_response :success
  end

  test "should update empresarial_ramo" do
    patch :update, id: @empresarial_ramo, empresarial_ramo: { ativo: @empresarial_ramo.ativo, nome: @empresarial_ramo.nome }
    assert_redirected_to empresarial_ramo_path(assigns(:empresarial_ramo))
  end

  test "should destroy empresarial_ramo" do
    assert_difference('EmpresarialRamo.count', -1) do
      delete :destroy, id: @empresarial_ramo
    end

    assert_redirected_to empresarial_ramos_path
  end
end
