require 'test_helper'

class ModulosPessoasControllerTest < ActionController::TestCase
  setup do
    @modulos_pessoa = modulos_pessoas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:modulos_pessoas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create modulos_pessoa" do
    assert_difference('ModulosPessoa.count') do
      post :create, modulos_pessoa: { ativo: @modulos_pessoa.ativo, modulo_id: @modulos_pessoa.modulo_id, permitido: @modulos_pessoa.permitido, pessoa_id: @modulos_pessoa.pessoa_id }
    end

    assert_redirected_to modulos_pessoa_path(assigns(:modulos_pessoa))
  end

  test "should show modulos_pessoa" do
    get :show, id: @modulos_pessoa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @modulos_pessoa
    assert_response :success
  end

  test "should update modulos_pessoa" do
    patch :update, id: @modulos_pessoa, modulos_pessoa: { ativo: @modulos_pessoa.ativo, modulo_id: @modulos_pessoa.modulo_id, permitido: @modulos_pessoa.permitido, pessoa_id: @modulos_pessoa.pessoa_id }
    assert_redirected_to modulos_pessoa_path(assigns(:modulos_pessoa))
  end

  test "should destroy modulos_pessoa" do
    assert_difference('ModulosPessoa.count', -1) do
      delete :destroy, id: @modulos_pessoa
    end

    assert_redirected_to modulos_pessoas_path
  end
end
