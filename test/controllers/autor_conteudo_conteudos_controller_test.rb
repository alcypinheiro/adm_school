require 'test_helper'

class AutorConteudoConteudosControllerTest < ActionController::TestCase
  setup do
    @autor_conteudo_conteudo = autor_conteudo_conteudos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:autor_conteudo_conteudos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create autor_conteudo_conteudo" do
    assert_difference('AutorConteudoConteudo.count') do
      post :create, autor_conteudo_conteudo: { ativo: @autor_conteudo_conteudo.ativo, autor_conteudo_id: @autor_conteudo_conteudo.autor_conteudo_id, conteudo_id: @autor_conteudo_conteudo.conteudo_id, nome: @autor_conteudo_conteudo.nome }
    end

    assert_redirected_to autor_conteudo_conteudo_path(assigns(:autor_conteudo_conteudo))
  end

  test "should show autor_conteudo_conteudo" do
    get :show, id: @autor_conteudo_conteudo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @autor_conteudo_conteudo
    assert_response :success
  end

  test "should update autor_conteudo_conteudo" do
    patch :update, id: @autor_conteudo_conteudo, autor_conteudo_conteudo: { ativo: @autor_conteudo_conteudo.ativo, autor_conteudo_id: @autor_conteudo_conteudo.autor_conteudo_id, conteudo_id: @autor_conteudo_conteudo.conteudo_id, nome: @autor_conteudo_conteudo.nome }
    assert_redirected_to autor_conteudo_conteudo_path(assigns(:autor_conteudo_conteudo))
  end

  test "should destroy autor_conteudo_conteudo" do
    assert_difference('AutorConteudoConteudo.count', -1) do
      delete :destroy, id: @autor_conteudo_conteudo
    end

    assert_redirected_to autor_conteudo_conteudos_path
  end
end
