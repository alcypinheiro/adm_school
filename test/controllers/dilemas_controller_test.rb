require 'test_helper'

class DilemasControllerTest < ActionController::TestCase
  setup do
    @dilema = dilemas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dilemas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dilema" do
    assert_difference('Dilema.count') do
      post :create, dilema: { ativo: @dilema.ativo, nome: @dilema.nome }
    end

    assert_redirected_to dilema_path(assigns(:dilema))
  end

  test "should show dilema" do
    get :show, id: @dilema
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dilema
    assert_response :success
  end

  test "should update dilema" do
    patch :update, id: @dilema, dilema: { ativo: @dilema.ativo, nome: @dilema.nome }
    assert_redirected_to dilema_path(assigns(:dilema))
  end

  test "should destroy dilema" do
    assert_difference('Dilema.count', -1) do
      delete :destroy, id: @dilema
    end

    assert_redirected_to dilemas_path
  end
end
