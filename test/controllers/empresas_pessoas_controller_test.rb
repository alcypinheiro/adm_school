require 'test_helper'

class EmpresasPessoasControllerTest < ActionController::TestCase
  setup do
    @empresas_pessoa = empresas_pessoas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:empresas_pessoas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create empresas_pessoa" do
    assert_difference('EmpresasPessoa.count') do
      post :create, empresas_pessoa: { ativo: @empresas_pessoa.ativo, empresa_id: @empresas_pessoa.empresa_id, pessoa_id: @empresas_pessoa.pessoa_id }
    end

    assert_redirected_to empresas_pessoa_path(assigns(:empresas_pessoa))
  end

  test "should show empresas_pessoa" do
    get :show, id: @empresas_pessoa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @empresas_pessoa
    assert_response :success
  end

  test "should update empresas_pessoa" do
    patch :update, id: @empresas_pessoa, empresas_pessoa: { ativo: @empresas_pessoa.ativo, empresa_id: @empresas_pessoa.empresa_id, pessoa_id: @empresas_pessoa.pessoa_id }
    assert_redirected_to empresas_pessoa_path(assigns(:empresas_pessoa))
  end

  test "should destroy empresas_pessoa" do
    assert_difference('EmpresasPessoa.count', -1) do
      delete :destroy, id: @empresas_pessoa
    end

    assert_redirected_to empresas_pessoas_path
  end
end
