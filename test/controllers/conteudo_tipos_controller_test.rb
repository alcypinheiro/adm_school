require 'test_helper'

class ConteudoTiposControllerTest < ActionController::TestCase
  setup do
    @conteudo_tipo = conteudo_tipos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:conteudo_tipos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create conteudo_tipo" do
    assert_difference('ConteudoTipo.count') do
      post :create, conteudo_tipo: { ativo: @conteudo_tipo.ativo, nome: @conteudo_tipo.nome }
    end

    assert_redirected_to conteudo_tipo_path(assigns(:conteudo_tipo))
  end

  test "should show conteudo_tipo" do
    get :show, id: @conteudo_tipo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @conteudo_tipo
    assert_response :success
  end

  test "should update conteudo_tipo" do
    patch :update, id: @conteudo_tipo, conteudo_tipo: { ativo: @conteudo_tipo.ativo, nome: @conteudo_tipo.nome }
    assert_redirected_to conteudo_tipo_path(assigns(:conteudo_tipo))
  end

  test "should destroy conteudo_tipo" do
    assert_difference('ConteudoTipo.count', -1) do
      delete :destroy, id: @conteudo_tipo
    end

    assert_redirected_to conteudo_tipos_path
  end
end
