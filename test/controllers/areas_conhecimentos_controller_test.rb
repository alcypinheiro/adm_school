require 'test_helper'

class AreasConhecimentosControllerTest < ActionController::TestCase
  setup do
    @areas_conhecimento = areas_conhecimentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:areas_conhecimentos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create areas_conhecimento" do
    assert_difference('AreasConhecimento.count') do
      post :create, areas_conhecimento: { ativo: @areas_conhecimento.ativo, nome: @areas_conhecimento.nome }
    end

    assert_redirected_to areas_conhecimento_path(assigns(:areas_conhecimento))
  end

  test "should show areas_conhecimento" do
    get :show, id: @areas_conhecimento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @areas_conhecimento
    assert_response :success
  end

  test "should update areas_conhecimento" do
    patch :update, id: @areas_conhecimento, areas_conhecimento: { ativo: @areas_conhecimento.ativo, nome: @areas_conhecimento.nome }
    assert_redirected_to areas_conhecimento_path(assigns(:areas_conhecimento))
  end

  test "should destroy areas_conhecimento" do
    assert_difference('AreasConhecimento.count', -1) do
      delete :destroy, id: @areas_conhecimento
    end

    assert_redirected_to areas_conhecimentos_path
  end
end
