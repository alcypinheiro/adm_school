require 'test_helper'

class MeusCursosControllerTest < ActionController::TestCase
  setup do
    @meus_curso = meus_cursos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:meus_cursos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create meus_curso" do
    assert_difference('MeusCurso.count') do
      post :create, meus_curso: { ativo: @meus_curso.ativo, conteudo_id: @meus_curso.conteudo_id, pessoa_id: @meus_curso.pessoa_id }
    end

    assert_redirected_to meus_curso_path(assigns(:meus_curso))
  end

  test "should show meus_curso" do
    get :show, id: @meus_curso
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @meus_curso
    assert_response :success
  end

  test "should update meus_curso" do
    patch :update, id: @meus_curso, meus_curso: { ativo: @meus_curso.ativo, conteudo_id: @meus_curso.conteudo_id, pessoa_id: @meus_curso.pessoa_id }
    assert_redirected_to meus_curso_path(assigns(:meus_curso))
  end

  test "should destroy meus_curso" do
    assert_difference('MeusCurso.count', -1) do
      delete :destroy, id: @meus_curso
    end

    assert_redirected_to meus_cursos_path
  end
end
