class ModulosPessoa < ActiveRecord::Base
  belongs_to :pessoa
  belongs_to :modulo
  has_many :autor_conteudos
end
