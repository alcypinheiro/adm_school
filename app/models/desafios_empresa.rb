class DesafiosEmpresa < ActiveRecord::Base
  belongs_to :empresa
  belongs_to :areas_conhecimento

  #MESTRE DETALHE

  # DILEMAS
  has_many :dilemas_desafios, inverse_of: :desafios_empresa, dependent: :destroy
  accepts_nested_attributes_for :dilemas_desafios, reject_if: :all_blank, allow_destroy: true

  # METODOLOGIAS 
  has_many :metodologias_desafios_empresas, inverse_of: :desafios_empresa, dependent: :destroy
  accepts_nested_attributes_for :metodologias_desafios_empresas, reject_if: :all_blank, allow_destroy: true
end
