class Empresa < ActiveRecord::Base
	belongs_to :usuario
	has_many :desafios_empresas

	def self.validar_instituicao(empresa,usuario)
		pessoa = Pessoa.find(usuario)
		EmpresasPessoa.create(pessoa_id: pessoa.id, empresa_id: empresa.id)
		
		if empresa.instituicao_ensino == false
			emp = Modulo.where(nome: "Empresa")
			if !emp.blank?
				ModulosPessoa.create(pessoa_id: pessoa.id, modulo_id: emp.first.id)
				return false
			end
		else
			ins = Modulo.where(nome: "Instituição de Ensino")
			if !ins.blank?
				ModulosPessoa.create(pessoa_id: pessoa.id, modulo_id: ins.first.id)
				return true
			end
		end
	end
end
