class Conteudo < ActiveRecord::Base
  belongs_to :conteudo_tipo
  has_many :autor_conteudo
  belongs_to :canai

  # anexo de imagem
  has_attached_file :image,
                    :styles => { :medium => "200x200>",
                                 :thumb => "200x200>" }
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]}
  # Autor conteúdo
  has_many :autor_conteudo_conteudos, inverse_of: :conteudo, dependent: :destroy
  accepts_nested_attributes_for :autor_conteudo_conteudos, reject_if: :all_blank, allow_destroy: true
end
