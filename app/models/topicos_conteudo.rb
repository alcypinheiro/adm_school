class TopicosConteudo < ActiveRecord::Base
  belongs_to :formato_conteudo

  #anexo de video
  #has_attached_file :video
  #validates_attachment :video, content_type: { content_type: ["video/mp4", "video/wmv"]}
  
  #has_attached_file :video,
  #:storage => :cloudinary,
  #:cloudinary_resource_type => :video

  #carrierwave video upload
  mount_uploader :video, VideoUploader 

  # after_save :add_tag

  # def add_tag 
  #   if self.video.present?  
  #     result = Cloudinary::Uploader.add_tag(model.conteudo_id, model.id, options = {resource_type: video})
  #   end
  # end 
end
