class ApplicationMailer < ActionMailer::Base
  default from: 'gerencia.bem.agil@gmail.com'
  layout 'mailer'
end
