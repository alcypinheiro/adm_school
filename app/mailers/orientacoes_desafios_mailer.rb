class OrientacoesDesafiosMailer < ApplicationMailer
	default :from => 'gerencia.bem.agil@gmail.com'

	def orientacao(email, orientacoes)
    @email = email
    @orientacoes = orientacoes
    mail( :to => @email, :subject => 'Orientações para Lançar Desafio' )
  end
end
