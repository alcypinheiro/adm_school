class OrientacaoMailer < ApplicationMailer
	def orientacao_message(orientacoes_empresa)
    	@orientacoes_empresa = orientacoes_empresa
    	mail(to: @orientacoes_empresa.email, subject: 'Orientação Admschool')
  	end
end
