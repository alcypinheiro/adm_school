class ContactMailer < ApplicationMailer
	def contact_message(empresa)
    	@empresa = empresa
    	mail(to: @empresa.email, subject: 'Bem vindo!')
  	end
end