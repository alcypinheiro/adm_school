class DesafiosEmpresasController < ApplicationController
  before_action :set_desafios_empresa, only: [:show, :edit, :update, :destroy]
  
  # GET /desafios_empresas
  # GET /desafios_empresas.json
  def index
    @desafios_empresas = DesafiosEmpresa.where(ativo: true, empresa_id: (EmpresasPessoa.select(:empresa_id).where(ativo: true, pessoa_id: (Pessoa.select(:id).where(ativo: true, usuario_id: current_usuario)))))
    @status = StatusDesafio.where(ativo: true).count
  end

  # GET /desafios_empresas/1
  # GET /desafios_empresas/1.json
  def show
    @dilemas = DilemasDesafio.where(ativo: true, desafios_empresa_id: params[:id])
    @metodologias = MetodologiasDesafiosEmpresa.where(ativo: true, desafios_empresa_id: params[:id])
  end

  def todos_desafios
    @desafios_empresas = DesafiosEmpresa.where(ativo: true)
    @status = StatusDesafio.where(ativo: true).count
  end
  # GET /desafios_empresas/new
  def new
    @desafios_empresa = DesafiosEmpresa.new
    
    @dilema_desafio = @desafios_empresa.dilemas_desafios.build
    @metodologias_desafios_empresa = @desafios_empresa.metodologias_desafios_empresas.build

    @pessoa = Pessoa.where(usuario_id: current_usuario.id, ativo: true)
    if !@pessoa.blank?
      @empresa = Empresa.where(ativo: true, id: (EmpresasPessoa.select(:empresa_id).where(pessoa_id: @pessoa.first.id, ativo: true)))
    end
  end

  # GET /desafios_empresas/1/edit
  def edit
    @pessoa = Pessoa.where(usuario_id: current_usuario.id, ativo: true)
    if !@pessoa.blank?
      @empresa = Empresa.where(ativo: true, id: (EmpresasPessoa.select(:empresa_id).where(pessoa_id: @pessoa.first.id, ativo: true)))
    end
  end

  # POST /desafios_empresas
  # POST /desafios_empresas.json
  def create
    @desafios_empresa = DesafiosEmpresa.new(desafios_empresa_params)

    @status = StatusDesafio.where(sequencia: 1, ativo:true)
    if  !@status.blank?
      @desafios_empresa.status_desafio_id = @status.first.id 
      HistoricoStatusDesafio.create(status_desafio_id: @status.first.id, desafios_empresa_id: @desafios_empresa.id)
    end

    respond_to do |format|
      if @desafios_empresa.save
        if  !@status.blank?
          HistoricoStatusDesafio.create(status_desafio_id: @status.first.id, desafios_empresa_id: @desafios_empresa.id, data_inicio: Date.today)
        end
        format.html { redirect_to @desafios_empresa, notice: 'Desafio enviado para análise com sucesso.' }
        format.json { render action: 'show', status: :created, location: @desafios_empresa }
      else
        format.html { render action: 'new' }
        format.json { render json: @desafios_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /desafios_empresas/1
  # PATCH/PUT /desafios_empresas/1.json
  def update
    respond_to do |format|
      if @desafios_empresa.update(desafios_empresa_params)
        format.html { redirect_to @desafios_empresa, notice: 'Desafio alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @desafios_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /desafios_empresas/1
  # DELETE /desafios_empresas/1.json
  def destroy
    @desafios_empresa.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to desafios_empresas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_desafios_empresa
      @desafios_empresa = DesafiosEmpresa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def desafios_empresa_params
      params.require(:desafios_empresa).permit(:ensino, :profissional, :solucao, :empresa_id, :areas_conhecimento_id, :contextualizacao, :principais_problemas, :dados_mercado, :termo_autorizacao, :ativo,:resumo, dilemas_desafios_attributes: [:id, :desafios_empresa_id, :nome, :ativo, :_destroy], metodologias_desafios_empresas_attributes: [:id, :desafios_empresa_id, :nome, :ativo, :_destroy])
    end
end
