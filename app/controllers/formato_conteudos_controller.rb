class FormatoConteudosController < ApplicationController
  before_action :set_formato_conteudo, only: [:show, :edit, :update, :destroy]

  # GET /formato_conteudos
  # GET /formato_conteudos.json
  def index
    @formato_conteudos = FormatoConteudo.where(ativo: true)
  end

  # GET /formato_conteudos/1
  # GET /formato_conteudos/1.json
  def show
  end

  # GET /formato_conteudos/new
  def new
    @formato_conteudo = FormatoConteudo.new
  end

  # GET /formato_conteudos/1/edit
  def edit
  end

  # POST /formato_conteudos
  # POST /formato_conteudos.json
  def create
    @formato_conteudo = FormatoConteudo.new(formato_conteudo_params)

    respond_to do |format|
      if @formato_conteudo.save
        format.html { redirect_to @formato_conteudo, notice: 'Formato de conteúdo cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @formato_conteudo }
      else
        format.html { render action: 'new' }
        format.json { render json: @formato_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /formato_conteudos/1
  # PATCH/PUT /formato_conteudos/1.json
  def update
    respond_to do |format|
      if @formato_conteudo.update(formato_conteudo_params)
        format.html { redirect_to @formato_conteudo, notice: 'Formato de conteúdo alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @formato_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /formato_conteudos/1
  # DELETE /formato_conteudos/1.json
  def destroy
    @formato_conteudo.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to formato_conteudos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_formato_conteudo
      @formato_conteudo = FormatoConteudo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def formato_conteudo_params
      params.require(:formato_conteudo).permit(:nome, :ativo)
    end
end
