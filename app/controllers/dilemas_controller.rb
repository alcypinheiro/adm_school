class DilemasController < ApplicationController
  before_action :set_dilema, only: [:show, :edit, :update, :destroy]

  # GET /dilemas
  # GET /dilemas.json
  def index
    @dilemas = Dilema.where(ativo: true)
  end

  # GET /dilemas/1
  # GET /dilemas/1.json
  def show
  end

  # GET /dilemas/new
  def new
    @dilema = Dilema.new
  end

  # GET /dilemas/1/edit
  def edit
  end

  # POST /dilemas
  # POST /dilemas.json
  def create
    @dilema = Dilema.new(dilema_params)

    respond_to do |format|
      if @dilema.save
        format.html { redirect_to @dilema, notice: 'Dilema cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @dilema }
      else
        format.html { render action: 'new' }
        format.json { render json: @dilema.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dilemas/1
  # PATCH/PUT /dilemas/1.json
  def update
    respond_to do |format|
      if @dilema.update(dilema_params)
        format.html { redirect_to @dilema, notice: 'Dilema alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @dilema.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dilemas/1
  # DELETE /dilemas/1.json
  def destroy
    @dilema.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to dilemas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dilema
      @dilema = Dilema.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dilema_params
      params.require(:dilema).permit(:nome, :ativo)
    end
end
