class TopicosConteudosController < ApplicationController
  before_action :set_topicos_conteudo, only: [:show, :edit, :update, :destroy]

  # GET /topicos_conteudos
  # GET /topicos_conteudos.json
  def index
    @topicos_conteudos = TopicosConteudo.where(ativo: true)
  end

  # GET /topicos_conteudos/1
  # GET /topicos_conteudos/1.json
  def show
  end

  # GET /topicos_conteudos/new
  def new
    @topicos_conteudo = TopicosConteudo.new
  end

  # GET /topicos_conteudos/1/edit
  def edit
  end

  # POST /topicos_conteudos
  # POST /topicos_conteudos.json
  def create
    @topico = TopicosConteudo.where(conteudo_id: params[:topicos_conteudo][:conteudo_id], ativo: true)
    @topicos_conteudo = TopicosConteudo.new(topicos_conteudo_params)
    #@topicos_conteudo.video = params[:topicos_conteudo][:video]

    respond_to do |format|
      if @topicos_conteudo.save
        # format.html do
        #   redirect_to salva_tag_salvar_tags_path(topicos_conteudo_id: @topicos_conteudo.id, conteudo_id:  @topicos_conteudo.conteudo_id)
        # end
        #result = Cloudinary::Uploader.rename(from_topicos_conteudo_id, to_topicos_conteudo_id, options = {})
        
        # format.html { redirect_to @topicos_conteudo, notice: 'Tópico do conteúdo cadastrado com sucesso.' }
        # format.json { render action: 'show', status: :created, location: @topicos_conteudo }
        format.html { redirect_to pages_topico_conteudo_path(conteudo_id: params[:topicos_conteudo][:conteudo_id], ativo: true) }
      else
        format.html { render action: 'new' }
        format.json { render json: @topicos_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  def topicos_params
    params.require(:topicos_conteudo).permit(:video)
  end

  # PATCH/PUT /topicos_conteudos/1
  # PATCH/PUT /topicos_conteudos/1.json
  def update
    respond_to do |format|
      if @topicos_conteudo.update(topicos_conteudo_params)
        #@topicos_conteudo.video = params[:topicos_conteudo][:video]
        format.html { redirect_to @topicos_conteudo, notice: 'Tópico do conteúdo alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @topicos_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topicos_conteudos/1
  # DELETE /topicos_conteudos/1.json
  def destroy
    @topicos_conteudo.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to topicos_conteudos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topicos_conteudo
      @topicos_conteudo = TopicosConteudo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topicos_conteudo_params
      params.require(:topicos_conteudo).permit(:descricao, :video, :link, :objetivo, :resumo, :conteudo_id, :formato_conteudo_id, :ativo)
    end
end
