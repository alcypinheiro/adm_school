class EmpresarialRamosController < ApplicationController
  before_action :set_empresarial_ramo, only: [:show, :edit, :update, :destroy]

  # GET /empresarial_ramos
  # GET /empresarial_ramos.json
  def index
    @empresarial_ramos = EmpresarialRamo.where(ativo: true)
  end

  # GET /empresarial_ramos/1
  # GET /empresarial_ramos/1.json
  def show
  end

  # GET /empresarial_ramos/new
  def new
    @empresarial_ramo = EmpresarialRamo.new
  end

  # GET /empresarial_ramos/1/edit
  def edit
  end

  # POST /empresarial_ramos
  # POST /empresarial_ramos.json
  def create
    @empresarial_ramo = EmpresarialRamo.new(empresarial_ramo_params)

    respond_to do |format|
      if @empresarial_ramo.save
        format.html { redirect_to @empresarial_ramo, notice: 'Ramo empresarial cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @empresarial_ramo }
      else
        format.html { render action: 'new' }
        format.json { render json: @empresarial_ramo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /empresarial_ramos/1
  # PATCH/PUT /empresarial_ramos/1.json
  def update
    respond_to do |format|
      if @empresarial_ramo.update(empresarial_ramo_params)
        format.html { redirect_to @empresarial_ramo, notice: 'Ramo empresarial alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @empresarial_ramo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empresarial_ramos/1
  # DELETE /empresarial_ramos/1.json
  def destroy
    @empresarial_ramo.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to empresarial_ramos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_empresarial_ramo
      @empresarial_ramo = EmpresarialRamo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def empresarial_ramo_params
      params.require(:empresarial_ramo).permit(:nome, :ativo)
    end
end
