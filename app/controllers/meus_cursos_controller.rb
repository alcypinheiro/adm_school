class MeusCursosController < ApplicationController
  before_action :set_meus_curso, only: [:show, :edit, :update, :destroy]

  # GET /meus_cursos
  # GET /meus_cursos.json
  def index
    @meus_cursos = MeusCurso.where(ativo: true)
  end

  # GET /meus_cursos/1
  # GET /meus_cursos/1.json
  def show
  end

  # GET /meus_cursos/new
  def new
    @meus_curso = MeusCurso.new
  end

  # GET /meus_cursos/1/edit
  def edit
  end

  def visualizar_curso
    @meus_curso = Conteudo.find_by(ativo: true, id: params[:conteudo_id])
    render pages_visualizar_curso_path
  end

  # POST /meus_cursos
  # POST /meus_cursos.json
  def create
    @meus_curso = MeusCurso.new(meus_curso_params)

    respond_to do |format|
      if @meus_curso.save
        format.html { redirect_to @meus_curso, notice: 'Curso cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @meus_curso }
      else
        format.html { render action: 'new' }
        format.json { render json: @meus_curso.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meus_cursos/1
  # PATCH/PUT /meus_cursos/1.json
  def update
    respond_to do |format|
      if @meus_curso.update(meus_curso_params)
        format.html { redirect_to @meus_curso, notice: 'Curso alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @meus_curso.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meus_cursos/1
  # DELETE /meus_cursos/1.json
  def destroy
    @meus_curso.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to meus_cursos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meus_curso
      @meus_curso = MeusCurso.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meus_curso_params
      params.require(:meus_curso).permit(:pessoa_id, :conteudo_id, :ativo)
    end
end
