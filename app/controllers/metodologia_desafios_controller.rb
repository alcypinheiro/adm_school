class MetodologiaDesafiosController < ApplicationController
  before_action :set_metodologia_desafio, only: [:show, :edit, :update, :destroy]

  # GET /metodologia_desafios
  # GET /metodologia_desafios.json
  def index
    @metodologia_desafios = MetodologiaDesafio.where(ativo: true)
  end

  # GET /metodologia_desafios/1
  # GET /metodologia_desafios/1.json
  def show
  end

  # GET /metodologia_desafios/new
  def new
    @metodologia_desafio = MetodologiaDesafio.new
  end

  # GET /metodologia_desafios/1/edit
  def edit
  end

  # POST /metodologia_desafios
  # POST /metodologia_desafios.json
  def create
    @metodologia_desafio = MetodologiaDesafio.new(metodologia_desafio_params)

    respond_to do |format|
      if @metodologia_desafio.save
        format.html { redirect_to @metodologia_desafio, notice: 'Metodologia cadastrada com sucesso.' }
        format.json { render action: 'show', status: :created, location: @metodologia_desafio }
      else
        format.html { render action: 'new' }
        format.json { render json: @metodologia_desafio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /metodologia_desafios/1
  # PATCH/PUT /metodologia_desafios/1.json
  def update
    respond_to do |format|
      if @metodologia_desafio.update(metodologia_desafio_params)
        format.html { redirect_to @metodologia_desafio, notice: 'Metodologia alterada com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @metodologia_desafio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /metodologia_desafios/1
  # DELETE /metodologia_desafios/1.json
  def destroy
    @metodologia_desafio.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to metodologia_desafios_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_metodologia_desafio
      @metodologia_desafio = MetodologiaDesafio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def metodologia_desafio_params
      params.require(:metodologia_desafio).permit(:nome, :ativo)
    end
end
