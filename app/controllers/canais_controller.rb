class CanaisController < ApplicationController
  before_action :set_canai, only: [:show, :edit, :update, :destroy]

  # GET /canais
  # GET /canais.json
  def index
    @canais = Canai.where(ativo: true)
  end

  # GET /canais/1
  # GET /canais/1.json
  def show
  end

  def ver_conteudos_canal
    @conteudo_canal = Conteudo.where(ativo: true, canai_id: params[:canai_id])
    #Conteudo.joins(:canai).where("conteudos.ativo = ? AND")
    render pages_conteudos_canal_path
  end

  # GET /canais/new
  def new
    @canai = Canai.new
  end

  # GET /canais/1/edit
  def edit
  end

  # POST /canais
  # POST /canais.json
  def create
    @canai = Canai.new(canai_params)

    respond_to do |format|
      if @canai.save
        format.html { redirect_to @canai, notice: 'Canal cadastrado com sucesso.'}
        format.json { render action: 'show', status: :created, location: @canai }
      else
        format.html { render action: 'new' }
        format.json { render json: @canai.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /canais/1
  # PATCH/PUT /canais/1.json
  def update
    respond_to do |format|
      if @canai.update(canai_params)
        format.html { redirect_to @canai, notice: 'Canal alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @canai.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /canais/1
  # DELETE /canais/1.json
  def destroy
    @canai.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to canais_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_canai
      @canai = Canai.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def canai_params
      params.require(:canai).permit(:nome, :objetivo, :data_criacao, :empresa_id, :pessoa_id, :ativo)
    end
end
