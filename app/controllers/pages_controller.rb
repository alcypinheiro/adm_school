class PagesController < ApplicationController
  def search_results
  end

  def lockscreen
    render :layout => "empty"
  end

  def invoice
  end

  def invoice_print
    render :layout => "empty"
  end

  def login
    render :layout => "empty"
  end

  def acessar_curso
  end

  def login_2
    render :layout => "empty"
  end

  def forgot_password
      render :layout => "empty"
  end

  def register
    render :layout => "empty"
  end

  def assistir_curso
    @tag = params[:conteudo_id]
  end

  def topico_conteudo
    @topico = TopicosConteudo.where(conteudo_id: params[:conteudo_id], ativo: true)
    render pages_topicos_conteudo_path
  end

  def show_topico
    @topico = TopicosConteudo.find_by(id: params[:id], ativo: true)
    render pages_show_topicos_path
  end

  def show_problemas
    @tipo_conteudo = ConteudoTipo.joins(:conteudos).where("conteudos.ativo = ? AND conteudo_tipos.ativo = ?", true, true)
    render 
  end
  def busca_conteudo

    if params[:search].blank?  
      #render 'conteudo_error.html.erb'
      redirect_to(root_path)
    else  
      @parameter = params[:search].downcase  
      @conteudo_busca = Conteudo.all.where("lower(nome) LIKE :search AND ativo = true", search: @parameter) 

      render 'conteudo_busca.html.erb'
    end 


    # @conteudo_busca = Conteudo.search(params[:search]).where("nome LIKE ?", "%#{search}%", "%#{search}%")

    # if !@conteudo_busca.blank?
    #   render 'busca_conteudo.html.erb'
    # else
    #   render 'conteudo_error.html.erb'
    # end
    #@tipo_conteudo = ConteudoTipo.joins(:conteudos).where("conteudos.ativo = ? AND conteudo_tipos.ativo = ?", true, true)
    #@conteudo_busca = Conteudo.where(ativo: true, nome: params[:nome]).first
  end

  def modulo_gerador
    @paineis = Conteudo.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true))
    @modulos_pessoa = ModulosPessoa.new
    @modulos_pessoa.pessoa_id = current_usuario.pessoa.id
    @modulos_pessoa.modulo_id = Modulo.find_by(nome: "Gerador de conteúdo", ativo: true).id
    @modulos_pessoa.save
    if @modulos_pessoa.save
      @autor_conteudo = AutorConteudo.new
      @autor_conteudo.modulos_pessoa_id = ModulosPessoa.where(id: @modulos_pessoa.id, ativo: true).first.id
      @autor_conteudo.save

      render pages_painel_gerador_path
    end
  end

  def index
    # beginning_of_week = Date.today.beginning_of_week
    # beginning_of_next_week = beginning_of_week.next_week
    #@lancamentos = Conteudo.where(ativo: true, created_at: beginning_of_week..beginning_of_next_week)
    @lancamentos = Conteudo.where(ativo: true)
    @minha_listum = MinhaListum.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id)
    @carrinhos = Carrinho.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id).first

    #@busca_carrinho = ItensCarrinho.where(ativo: true, carrinho_id: @carrinhos.id)
    @busca_carrinho = ItensCarrinho.joins(:carrinho).where("carrinhos.ativo = ? AND carrinhos.pessoa_id = ? AND itens_carrinhos.ativo = ?", true, Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id, true )
  end

  def curso
    @lancamentos = Conteudo.where(ativo: true)
    @busca_carrinho = Carrinho.where(ativo:true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id)
    @carrinhos = Carrinho.where(ativo: true)
  end
  def painel
    @paineis = Conteudo.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true))
    render pages_painel_gerador_path
  end
  def internal_server_error
    render :layout => "empty"
  end

  def empty_page
  end

  def not_found_error
    render :layout => "empty"
  end

  def escolher_empresa
    render :layout => "empty"
  end

  def escolher_modulo
    render :layout => "empty"
  end

  def escolher_funcao
    render :layout => "empty"
  end
end
