class AutorConteudoConteudosController < ApplicationController
  before_action :set_autor_conteudo_conteudo, only: [:show, :edit, :update, :destroy]

  # GET /autor_conteudo_conteudos
  # GET /autor_conteudo_conteudos.json
  def index
    @autor_conteudo_conteudos = AutorConteudoConteudo.where(ativo: true)
  end

  # GET /autor_conteudo_conteudos/1
  # GET /autor_conteudo_conteudos/1.json
  def show
  end

  # GET /autor_conteudo_conteudos/new
  def new
    @autor_conteudo_conteudo = AutorConteudoConteudo.new
  end

  # GET /autor_conteudo_conteudos/1/edit
  def edit
  end

  # POST /autor_conteudo_conteudos
  # POST /autor_conteudo_conteudos.json
  def create
    #raise "akiautorconteudoconteudo".inspect
    @autor_conteudo_conteudo = AutorConteudoConteudo.new(autor_conteudo_conteudo_params)

    respond_to do |format|
      if @autor_conteudo_conteudo.save
        format.html { redirect_to @autor_conteudo_conteudo, notice: 'Autor conteudo conteudo was successfully created.' }
        format.json { render action: 'show', status: :created, location: @autor_conteudo_conteudo }
      else
        format.html { render action: 'new' }
        format.json { render json: @autor_conteudo_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /autor_conteudo_conteudos/1
  # PATCH/PUT /autor_conteudo_conteudos/1.json
  def update
    respond_to do |format|
      if @autor_conteudo_conteudo.update(autor_conteudo_conteudo_params)
        format.html { redirect_to @autor_conteudo_conteudo, notice: 'Autor conteudo conteudo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @autor_conteudo_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /autor_conteudo_conteudos/1
  # DELETE /autor_conteudo_conteudos/1.json
  def destroy
    @autor_conteudo_conteudo.destroy
    respond_to do |format|
      format.html { redirect_to autor_conteudo_conteudos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_autor_conteudo_conteudo
      @autor_conteudo_conteudo = AutorConteudoConteudo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def autor_conteudo_conteudo_params
      params.require(:autor_conteudo_conteudo).permit(:nome, :autor_conteudo_id, :conteudo_id, :ativo)
    end
end
