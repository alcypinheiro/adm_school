class ModulosPessoasController < ApplicationController
  before_action :set_modulos_pessoa, only: [:show, :edit, :update, :destroy]

  # GET /modulos_pessoas
  # GET /modulos_pessoas.json
  def index
    @modulos_pessoas = ModulosPessoa.where(ativo: true)
  end

  def escolher_modulo
    @modulos = ModulosPessoa.select(:modulo_id).distinct.where(ativo: true, pessoa_id: (Pessoa.select(:id).where(ativo: true, usuario_id: current_usuario.id)))
    render :layout => "empty"    
  end

  def setar_modulo
    session[:modulo_id] = params[:modulo_id]
    render :text => "Módulo #{Modulo.find(session[:modulo_id]).nome.upcase} selecionado com sucesso!"
  end
  # GET /modulos_pessoas/1
  # GET /modulos_pessoas/1.json
  def show
  end

  # GET /modulos_pessoas/new
  def new
    @modulos_pessoa = ModulosPessoa.new
  end

  # GET /modulos_pessoas/1/edit
  def edit
  end

  # POST /modulos_pessoas
  # POST /modulos_pessoas.json
  def create
    @modulos_pessoa = ModulosPessoa.new(modulos_pessoa_params)

    respond_to do |format|
      if @modulos_pessoa.save
        format.html { redirect_to @modulos_pessoa, notice: 'Módulo da Pessoa cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @modulos_pessoa }
      else
        format.html { render action: 'new' }
        format.json { render json: @modulos_pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /modulos_pessoas/1
  # PATCH/PUT /modulos_pessoas/1.json
  def update
    respond_to do |format|
      if @modulos_pessoa.update(modulos_pessoa_params)
        format.html { redirect_to @modulos_pessoa, notice: 'Módulo da Pessoa alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @modulos_pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /modulos_pessoas/1
  # DELETE /modulos_pessoas/1.json
  def destroy
    @modulos_pessoa.destroy
    respond_to do |format|
      format.html { redirect_to modulos_pessoas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modulos_pessoa
      @modulos_pessoa = ModulosPessoa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def modulos_pessoa_params
      params.require(:modulos_pessoa).permit(:pessoa_id, :modulo_id, :ativo)
    end
end
