class PessoasController < ApplicationController
  before_action :set_pessoa, only: [:show, :edit, :update, :destroy]

  # GET /pessoas
  # GET /pessoas.json
  def index
    @pessoas = Pessoa.where(ativo: true)
  end

  # GET /pessoas/1
  # GET /pessoas/1.json
  def show
  end

  # GET /pessoas/new
  def new
    @pessoa = Pessoa.new
  end

  # GET /pessoas/1/edit
  def edit
  end

  # POST /pessoas
  # POST /pessoas.json
  def create
    @pessoa = Pessoa.new(pessoa_params)
    @pessoa.usuario_id = current_usuario.id
    if @pessoa.save 
      @modulos_pessoa = ModulosPessoa.new
      @modulos_pessoa.pessoa_id = @pessoa.id
      @modulo = Modulo.find_by(nome: "Aluno", ativo: true)
      @modulos_pessoa.modulo_id = @modulo.id
      session[:aluno] = @modulo.nome
    end
    respond_to do |format|
      if @modulos_pessoa.save
        # format.html { redirect_to @pessoa, notice: 'Perfil cadastrado com sucesso.' }
        # format.json { render action: 'show', status: :created, location: @pessoa }
        format.html { redirect_to pages_index_path}
      else
        format.html { render action: 'new' }
        format.json { render json: @pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pessoas/1
  # PATCH/PUT /pessoas/1.json
  def update
    respond_to do |format|
      if @pessoa.update(pessoa_params)
        format.html { redirect_to @pessoa, notice: 'Perfil alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pessoas/1
  # DELETE /pessoas/1.json
  def destroy
    @pessoa.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to pessoas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pessoa
      @pessoa = Pessoa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pessoa_params
      params.require(:pessoa).permit(:nome, :email, :telefone, :cidade, :estado, :ativo, :usuario_id)
    end
end
