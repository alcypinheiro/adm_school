class OrientacoesDesafiosController < ApplicationController
  before_action :set_orientacoes_desafio, only: [:show, :edit, :update, :destroy]

  # GET /orientacoes_desafios
  # GET /orientacoes_desafios.json
  def index
    @orientacoes_desafios = OrientacoesDesafio.all
    @desafiar = Empresa.all.where.not(id: DesafiosEmpresa.select(:empresa_id).where(ativo: true))
  end

  # GET /orientacoes_desafios/1
  # GET /orientacoes_desafios/1.json
  def show
  end

  # GET /orientacoes_desafios/new
  def new
    @orientacoes_desafio = OrientacoesDesafio.new
  end

  # GET /orientacoes_desafios/1/edit
  def edit
  end

  # POST /orientacoes_desafios
  # POST /orientacoes_desafios.json
  def create
    @orientacoes_desafio = OrientacoesDesafio.new(orientacoes_desafio_params)

    respond_to do |format|
      if @orientacoes_desafio.save
        format.html { redirect_to @orientacoes_desafio, notice: 'Orientações do Desafios cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @orientacoes_desafio }
      else
        format.html { render action: 'new' }
        format.json { render json: @orientacoes_desafio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orientacoes_desafios/1
  # PATCH/PUT /orientacoes_desafios/1.json
  def update
    respond_to do |format|
      if @orientacoes_desafio.update(orientacoes_desafio_params)
        format.html { redirect_to @orientacoes_desafio, notice: 'Orientações dos Desafios alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @orientacoes_desafio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orientacoes_desafios/1
  # DELETE /orientacoes_desafios/1.json
  def destroy
    @orientacoes_desafio.destroy
    respond_to do |format|
      format.html { redirect_to orientacoes_desafios_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orientacoes_desafio
      @orientacoes_desafio = OrientacoesDesafio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orientacoes_desafio_params
      params.require(:orientacoes_desafio).permit(:desafios_empresa_id, :orientacao, :ativo)
    end
end
