class AutorConteudosController < ApplicationController
  before_action :set_autor_conteudo, only: [:show, :edit, :update, :destroy]

  # GET /autor_conteudos
  # GET /autor_conteudos.json
  def index
    @autor_conteudos = AutorConteudo.where(ativo: true)
  end

  # GET /autor_conteudos/1
  # GET /autor_conteudos/1.json
  def show
  end

  # GET /autor_conteudos/new
  def new
    @autor_conteudo = AutorConteudo.new
  end

  # GET /autor_conteudos/1/edit
  def edit
  end

  # POST /autor_conteudos
  # POST /autor_conteudos.json
  def create
    @autor_conteudo = AutorConteudo.new(autor_conteudo_params)

    respond_to do |format|
      if @autor_conteudo.save
        format.html { redirect_to @autor_conteudo, notice: 'Autor de conteúdo cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @autor_conteudo }
      else
        format.html { render action: 'new' }
        format.json { render json: @autor_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /autor_conteudos/1
  # PATCH/PUT /autor_conteudos/1.json
  def update
    respond_to do |format|
      if @autor_conteudo.update(autor_conteudo_params)
        format.html { redirect_to @autor_conteudo, notice: 'Autor de conteúdo alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @autor_conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /autor_conteudos/1
  # DELETE /autor_conteudos/1.json
  def destroy
    @autor_conteudo.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to autor_conteudos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_autor_conteudo
      @autor_conteudo = AutorConteudo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def autor_conteudo_params
      params.require(:autor_conteudo).permit(:nome, :modulos_pessoa_id, :ativo)
    end
end
