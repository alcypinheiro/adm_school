class EmpresasPessoasController < ApplicationController
  before_action :set_empresas_pessoa, only: [:show, :edit, :update, :destroy]

  # GET /empresas_pessoas
  # GET /empresas_pessoas.json
  def index
    @empresas_pessoas = EmpresasPessoa.where(ativo: true)
  end

  # GET /empresas_pessoas/1
  # GET /empresas_pessoas/1.json
  def show
  end

  # GET /empresas_pessoas/new
  def new
    @empresas_pessoa = EmpresasPessoa.new
  end

  # GET /empresas_pessoas/1/edit
  def edit
  end

  # POST /empresas_pessoas
  # POST /empresas_pessoas.json
  def create
    @empresas_pessoa = EmpresasPessoa.new(empresas_pessoa_params)

    respond_to do |format|
      if @empresas_pessoa.save
        format.html { redirect_to @empresas_pessoa, notice: 'O vínculo da pessoa com a empresa foi estabelecido com sucesso.' }
        format.json { render action: 'show', status: :created, location: @empresas_pessoa }
      else
        format.html { render action: 'new' }
        format.json { render json: @empresas_pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /empresas_pessoas/1
  # PATCH/PUT /empresas_pessoas/1.json
  def update
    respond_to do |format|
      if @empresas_pessoa.update(empresas_pessoa_params)
        format.html { redirect_to @empresas_pessoa, notice: 'O vínculo da pessoa com a empresa foi alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @empresas_pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empresas_pessoas/1
  # DELETE /empresas_pessoas/1.json
  def destroy
    @empresas_pessoa.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to empresas_pessoas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_empresas_pessoa
      @empresas_pessoa = EmpresasPessoa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def empresas_pessoa_params
      params.require(:empresas_pessoa).permit(:pessoa_id, :empresa_id, :ativo)
    end
end
