class PreLoginController < ApplicationController
	skip_before_action :authenticate_usuario!

	def index
		render :layout => "empty"
		beginning_of_week = Date.today.beginning_of_week
    beginning_of_next_week = beginning_of_week.next_week
    @lancamentos = Conteudo.where(ativo: true, created_at: beginning_of_week..beginning_of_next_week)

	end
end
