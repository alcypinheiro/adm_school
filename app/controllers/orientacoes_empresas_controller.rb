class OrientacoesEmpresasController < ApplicationController
  before_action :set_orientacoes_empresa, only: [:show, :edit, :update, :destroy]

  # GET /orientacoes_empresas
  # GET /orientacoes_empresas.json
  def index
    @orientacoes_empresas = OrientacoesEmpresa.where(ativo: true)
  end

  # GET /orientacoes_empresas/1
  # GET /orientacoes_empresas/1.json
  def show
  end

  # GET /orientacoes_empresas/new
  def new
    @orientacoes_empresa = OrientacoesEmpresa.new
  end

  # GET /orientacoes_empresas/1/edit
  def edit
  end

  # POST /orientacoes_empresas
  # POST /orientacoes_empresas.json
  def create
    @orientacoes_empresa = OrientacoesEmpresa.new(orientacoes_empresa_params)

    respond_to do |format|
      if @orientacoes_empresa.save
        OrientacaoMailer.orientacao_message(@orientacoes_empresa).deliver_now
        format.html { redirect_to @orientacoes_empresa, notice: 'Solicitação de orientação enviada com sucesso.' }
        format.json { render action: 'show', status: :created, location: @orientacoes_empresa }
      else
        format.html { render action: 'new' }
        format.json { render json: @orientacoes_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orientacoes_empresas/1
  # PATCH/PUT /orientacoes_empresas/1.json
  def update
    respond_to do |format|
      if @orientacoes_empresa.update(orientacoes_empresa_params)
        format.html { redirect_to @orientacoes_empresa, notice: 'Solicitação de orientação alterada com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @orientacoes_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orientacoes_empresas/1
  # DELETE /orientacoes_empresas/1.json
  def destroy
    @orientacoes_empresa.destroy
    respond_to do |format|
      format.html { redirect_to orientacoes_empresas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orientacoes_empresa
      @orientacoes_empresa = OrientacoesEmpresa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orientacoes_empresa_params
      params.require(:orientacoes_empresa).permit(:nome, :email, :telefone, :texto)
    end
end
