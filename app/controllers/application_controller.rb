class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_action :authenticate_usuario!
	layout :layout_by_resource
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end

  def after_sign_in_path_for(resource)
    pessoa = Pessoa.where(ativo: true, usuario_id: current_usuario.id)
    if pessoa.blank?
      new_pessoa_path
    else
      modulos_pessoas = ModulosPessoa.where(ativo: true, pessoa_id: pessoa.first.id)
      modulos_pessoas.each do |modulos_pessoa|

        if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Aluno"
          session[:aluno] = modulos_pessoa.modulo_id
        end

        if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Gerador de conteúdo"
          session[:geradorconteudo] = modulos_pessoa.modulo_id
        end
        
        if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Professor"
          session[:professor] = modulos_pessoa.modulo_id
        end

        if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Empresa"
          session[:empresa] = modulos_pessoa.modulo_id
        end

        if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Administrador"
          session[:administrador] = modulos_pessoa.modulo_id
        end

        # if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Aluno"
        #   if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Gerador de conteúdo"
        #     session[:geradorconteudo] = modulos_pessoas.first.modulo_id
        #     if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Professor"
        #       session[:professor] = modulos_pessoas.first.modulo_id
        #     end
        #   elsif Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Professor"
        #     session[:professor] = modulos_pessoas.first.modulo_id
            
        #     if Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Gerador de conteúdo"
        #       session[:geradorconteudo] = modulos_pessoas.first.modulo_id
        #       session[:professor] = modulos_pessoas.first.modulo_id
        #     else
        #     end
        #   end
        #   session[:aluno] = modulos_pessoas.first.modulo_id

        # elsif Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Empresa"
        #   session[:empresa] = modulos_pessoas.first.modulo_id
        # elsif Modulo.find_by(id: modulos_pessoa.modulo_id, ativo: true).nome == "Administrador"
        #   session[:administrador] = modulos_pessoas.first.modulo_id
        # end
      end
      pages_index_path
    end  
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:nome_usuario])
  end
end
