class StatusDesafiosController < ApplicationController
  before_action :set_status_desafio, only: [:show, :edit, :update, :destroy]

  # GET /status_desafios
  # GET /status_desafios.json
  def index
    @status_desafios = StatusDesafio.where(ativo: true)
  end

  # GET /status_desafios/1
  # GET /status_desafios/1.json
  def show
  end

  # GET /status_desafios/new
  def new
    @status_desafio = StatusDesafio.new
  end

  # GET /status_desafios/1/edit
  def edit
  end

  # POST /status_desafios
  # POST /status_desafios.json
  def create
    @status_desafio = StatusDesafio.new(status_desafio_params)

    respond_to do |format|
      if @status_desafio.save
        format.html { redirect_to @status_desafio, notice: 'Status dos Desafios cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @status_desafio }
      else
        format.html { render action: 'new' }
        format.json { render json: @status_desafio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /status_desafios/1
  # PATCH/PUT /status_desafios/1.json
  def update
    respond_to do |format|
      if @status_desafio.update(status_desafio_params)
        format.html { redirect_to @status_desafio, notice: 'Status dos Desafios alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @status_desafio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /status_desafios/1
  # DELETE /status_desafios/1.json
  def destroy
    @status_desafio.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to status_desafios_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status_desafio
      @status_desafio = StatusDesafio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def status_desafio_params
      params.require(:status_desafio).permit(:nome, :sequencia, :ativo)
    end
end
