class AreasConhecimentosController < ApplicationController
  before_action :set_areas_conhecimento, only: [:show, :edit, :update, :destroy]

  # GET /areas_conhecimentos
  # GET /areas_conhecimentos.json
  def index
    @areas_conhecimentos = AreasConhecimento.where(ativo: true)
  end

  # GET /areas_conhecimentos/1
  # GET /areas_conhecimentos/1.json
  def show
  end

  # GET /areas_conhecimentos/new
  def new
    @areas_conhecimento = AreasConhecimento.new
  end

  # GET /areas_conhecimentos/1/edit
  def edit
  end

  # POST /areas_conhecimentos
  # POST /areas_conhecimentos.json
  def create
    @areas_conhecimento = AreasConhecimento.new(areas_conhecimento_params)

    respond_to do |format|
      if @areas_conhecimento.save
        format.html { redirect_to @areas_conhecimento, notice: 'Área de conhecimento cadastrada com sucesso.' }
        format.json { render action: 'show', status: :created, location: @areas_conhecimento }
      else
        format.html { render action: 'new' }
        format.json { render json: @areas_conhecimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /areas_conhecimentos/1
  # PATCH/PUT /areas_conhecimentos/1.json
  def update
    respond_to do |format|
      if @areas_conhecimento.update(areas_conhecimento_params)
        format.html { redirect_to @areas_conhecimento, notice: 'Área de conhecimento alterada com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @areas_conhecimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /areas_conhecimentos/1
  # DELETE /areas_conhecimentos/1.json
  def destroy
    @areas_conhecimento.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to areas_conhecimentos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_areas_conhecimento
      @areas_conhecimento = AreasConhecimento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def areas_conhecimento_params
      params.require(:areas_conhecimento).permit(:nome, :ativo)
    end
end
