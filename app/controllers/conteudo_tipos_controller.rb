class ConteudoTiposController < ApplicationController
  before_action :set_conteudo_tipo, only: [:show, :edit, :update, :destroy]

  # GET /conteudo_tipos
  # GET /conteudo_tipos.json
  def index
    @conteudo_tipos = ConteudoTipo.where(ativo: true)
  end

  # GET /conteudo_tipos/1
  # GET /conteudo_tipos/1.json
  def show
  end

  # GET /conteudo_tipos/new
  def new
    @conteudo_tipo = ConteudoTipo.new
  end

  # GET /conteudo_tipos/1/edit
  def edit
  end

  # POST /conteudo_tipos
  # POST /conteudo_tipos.json
  def create
    @conteudo_tipo = ConteudoTipo.new(conteudo_tipo_params)

    respond_to do |format|
      if @conteudo_tipo.save
        format.html { redirect_to @conteudo_tipo, notice: 'Tipo de conteúdo cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @conteudo_tipo }
      else
        format.html { render action: 'new' }
        format.json { render json: @conteudo_tipo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conteudo_tipos/1
  # PATCH/PUT /conteudo_tipos/1.json
  def update
    respond_to do |format|
      if @conteudo_tipo.update(conteudo_tipo_params)
        format.html { redirect_to @conteudo_tipo, notice: 'Tipo de conteúdo alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @conteudo_tipo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conteudo_tipos/1
  # DELETE /conteudo_tipos/1.json
  def destroy
    @conteudo_tipo.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to conteudo_tipos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conteudo_tipo
      @conteudo_tipo = ConteudoTipo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conteudo_tipo_params
      params.require(:conteudo_tipo).permit(:nome, :ativo)
    end
end
