class CarrinhosController < ApplicationController
  before_action :set_carrinho, only: [:show, :edit, :update, :destroy]

  # GET /carrinhos
  # GET /carrinhos.json
  def index
    @itens_carrinhos = ItensCarrinho.joins(:carrinho).where("carrinhos.pessoa_id = ? AND carrinhos.ativo = ? AND itens_carrinhos.ativo = ?", current_usuario.id, true, true)

  end

  # GET /carrinhos/1
  # GET /carrinhos/1.json
  def show
  end

  # GET /carrinhos/new
  def new
    @carrinho = Carrinho.new
    #@product = ItensCarrinho.find(params[:product_id])
    #@session_id = (PagSeguro::Session.create).id
  end
  def add_carrinho
    if !Carrinho.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id).blank?
      if ItensCarrinho.where(conteudo_id: params[:conteudo_id], ativo: true).blank?
        @conteudo = Conteudo.find_by(id: params[:conteudo_id], ativo: true)
        @itens_carrinho = ItensCarrinho.new
        @itens_carrinho.nome = @conteudo.nome
        
        @itens_carrinho.preco_sugerido = @conteudo.preco_sugerido
        @itens_carrinho.conteudo_id = params[:conteudo_id]
        @itens_carrinho.carrinho_id = Carrinho.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id).last.id
        @itens_carrinho.save
        @notice_carrinho = true
        @notice = "Item adicionado ao carrinho!"
        @busca_carrinho = ItensCarrinho.where(ativo: true)
        render :inline => render_to_string(:partial => 'botao_carrinho')

      else
        @busca_carrinho = ItensCarrinho.where(ativo: true)
        @notice_pass_carrinho = true
        @notice = "Item já existente no carrinho!"
        render :inline => render_to_string(:partial => 'botao_carrinho')
      end  
      
    else
      @carrinhos = Carrinho.new
      @carrinhos.pessoa_id = Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id
      if @carrinhos.save
        if ItensCarrinho.where(conteudo_id: params[:conteudo_id]).blank?
          @conteudo = Conteudo.find_by(id: params[:conteudo_id], ativo: true)
          @itens_carrinho = ItensCarrinho.new
          @itens_carrinho.nome = @conteudo.nome
          
          @itens_carrinho.preco_sugerido = @conteudo.preco_sugerido
          @itens_carrinho.conteudo_id = params[:conteudo_id]
          @itens_carrinho.carrinho_id = Carrinho.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id).last.id
          @itens_carrinho.save
          @notice_pass_carrinho = true
          @notice = "Item adicionado ao carrinho"
          @busca_carrinho = ItensCarrinho.where(ativo: true)
          render :inline => render_to_string(:partial => 'botao_carrinho')

        else
          @busca_carrinho = ItensCarrinho.where(ativo: true)
          @notice_pass_carrinho = true
          @notice = "Item já existente no carrinho"
          render :inline => render_to_string(:partial => 'botao_carrinho')
        end  
      end
    end

  end

  def deleta_item
    @itens_carrinhos = ItensCarrinho.joins(:carrinho).where("carrinhos.pessoa_id = ? AND carrinhos.ativo = ? AND itens_carrinhos.ativo = ?", current_usuario.id, true, true)
    if @itens_carrinho = ItensCarrinho.find_by(id: params[:itens_carrinho_id], ativo: true)   
      @itens_carrinho.ativo = false
      @itens_carrinho.save

    end
    render :inline => render_to_string(:partial => 'carrinho.html.erb')
      #render '_carrinho.html.erb'
  end

  def finaliza_compra
    pessoa = Pessoa.find_by(usuario_id: current_usuario.id, ativo: true)
    carrinho = Carrinho.find_by(pessoa_id: pessoa.id, ativo: true)

    ItensCarrinho.where(ativo: true, carrinho_id: carrinho.id).each do |item_carrinho|
      meu_curso = MeusCurso.new
      meu_curso.conteudo_id = item_carrinho.conteudo_id
      meu_curso.pessoa_id = pessoa.id
      meu_curso.save
    end

    carrinho.update(concluido: true)

    redirect_to meus_cursos_path
  end

  # GET /carrinhos/1/edit
  def edit
  end

  # POST /carrinhos
  # POST /carrinhos.json
  def pagamento
    render pagamento_carrinhos_path
  end

  def create
    raise "qualquer coisa".inspect
    @carrinho = Carrinho.new(carrinho_params)

    @product = Product.find(params[:product_id])
 
    payment = PagSeguro::CreditCardTransactionRequest.new
    payment.notification_url = "https://secret-wave-53573.herokuapp.com/notification  "
    payment.payment_mode = "gateway"
 
    # Aqui vão os itens que serão cobrados na transação, caso você tenha multiplos itens
    # em um carrinho altere aqui para incluir sua lista de itens
    payment.items << { id: @product.id, description: @product.description, amount: @product.price, weight: 0 } # Criando uma referencia para a nossa ORDER reference = "REF_#{(0...8).map { (65 + rand(26)).chr }.join}_#{@product.id}" payment.reference = reference payment.sender = { hash: params[:sender_hash], name: params[:name], email: params[:email] } payment.credit_card_token = params[:card_token] payment.holder = { name: params[:card_name], birth_date: params[:birthday], document: { type: "CPF", value: params[:cpf] }, phone: { area_code: params[:phone_code], number: params[:phone_number] } } payment.installment = { value: @product.price, quantity: 1 } puts "=> REQUEST"
    puts PagSeguro::TransactionRequest::RequestSerializer.new(payment).to_params
    puts
 
    payment.create
 
    # Cria uma Order para registro das transações
    Order.create(product_id: @product.id, buyer_name: params[:name], reference: reference, status: 'pending')
 
    if payment.errors.any?
     puts "=> ERRORS"
     puts payment.errors.join("\n")
     render plain: "Erro No Pagamento #{payment.errors.join("\n")}"
    else
     puts "=> Transaction"
     puts "  code: #{payment.code}"
     puts "  reference: #{payment.reference}"
     puts "  type: #{payment.type_id}"
     puts "  payment link: #{payment.payment_link}"
     puts "  status: #{payment.status}"
     puts "  payment method type: #{payment.payment_method}"
     puts "  created at: #{payment.created_at}"
     puts "  updated at: #{payment.updated_at}"
     puts "  gross amount: #{payment.gross_amount.to_f}"
     puts "  discount amount: #{payment.discount_amount.to_f}"
     puts "  net amount: #{payment.net_amount.to_f}"
     puts "  extra amount: #{payment.extra_amount.to_f}"
     puts "  installment count: #{payment.installment_count}"
 
     puts "    => Items"
     puts "      items count: #{payment.items.size}"
     payment.items.each do |item|
       puts "      item id: #{item.id}"
       puts "      description: #{item.description}"
       puts "      quantity: #{item.quantity}"
       puts "      amount: #{item.amount.to_f}"
     end
 
     puts "    => Sender"
     puts "      name: #{payment.sender.name}"
     puts "      email: #{payment.sender.email}"
     puts "      phone: (#{payment.sender.phone.area_code}) #{payment.sender.phone.number}"
     puts "      document: #{payment.sender.document}: #{payment.sender.document}"
     render plain: "Sucesso, seu pagamento será processado :)"
    end

    respond_to do |format|
      if @carrinho.save
        format.html { redirect_to @carrinho, notice: 'Item do carrinho cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @carrinho }
      else
        format.html { render action: 'new' }
        format.json { render json: @carrinho.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carrinhos/1
  # PATCH/PUT /carrinhos/1.json
  def update
    respond_to do |format|
      if @carrinho.update(carrinho_params)
        format.html { redirect_to @carrinho, notice: 'Item do carrinho alterado com sucesso' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @carrinho.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carrinhos/1
  # DELETE /carrinhos/1.json
  def destroy
    @carrinho.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to carrinhos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_carrinho
      @carrinho = Carrinho.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def carrinho_params
      params.require(:carrinho).permit(:nome, :autor_conteudo_id, :preco_sugerido, :conteudo_id, :pessoa_id, :ativo)
    end
end
