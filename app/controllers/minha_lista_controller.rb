class MinhaListaController < ApplicationController
  before_action :set_minha_listum, only: [:show, :edit, :update, :destroy]

  # GET /minha_lista
  # GET /minha_lista.json
  def index
    @minha_lista = MinhaListum.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id)
  end

  # GET /minha_lista/1
  # GET /minha_lista/1.json
  def show
  end

  # GET /minha_lista/new
  def new
    @minha_listum = MinhaListum.new
  end

  def add_lista
    if MinhaListum.where(conteudo_id: params[:lancamentos_id], ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id).first.blank?
      @minha_listum = MinhaListum.new
      @minha_listum.conteudo_id = params[:lancamentos_id]
      @minha_listum.pessoa_id = Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id
      @minha_listum.save
      @notice_pass = true
      @notice =  "Adicionado à lista!"
      @minhas_listas = MinhaListum.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id)
      render :inline => render_to_string(:partial => 'minha_lista')
    else
      @notice_lista = true
      @aviso = "Item já existente na lista!"
      @minhas_listas = MinhaListum.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id)
      render :inline => render_to_string(:partial => 'minha_lista')
    end
  end

  def deleta_lista
    @minhas_listas = MinhaListum.where(ativo: true, pessoa_id: Pessoa.find_by(usuario_id: current_usuario.id, ativo: true).id)
    @minha_listum = MinhaListum.find(params[:minha_lista_id])
    @minha_listum.ativo = false
    @minha_listum.save
    render :inline => render_to_string(:partial => 'minha_lista')
  end

  # GET /minha_lista/1/edit
  def edit
  end

  # POST /minha_lista
  # POST /minha_lista.json
  def create
    @minha_listum = MinhaListum.new(minha_listum_params)

    respond_to do |format|
      if @minha_listum.save
        format.html { redirect_to @minha_listum, notice: 'Conteúdo adicionada à lista' }
        format.json { render action: 'show', status: :created, location: @minha_listum }
      else
        format.html { render action: 'new' }
        format.json { render json: @minha_listum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /minha_lista/1
  # PATCH/PUT /minha_lista/1.json
  def update
    respond_to do |format|
      if @minha_listum.update(minha_listum_params)
        format.html { redirect_to @minha_listum, notice: 'Conteúdo da lista alterado' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @minha_listum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /minha_lista/1
  # DELETE /minha_lista/1.json
  def destroy
    @minha_listum.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to minha_lista_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_minha_listum
      @minha_listum = MinhaListum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def minha_listum_params
      params.require(:minha_listum).permit(:conteudo_id, :pessoa_id, :ativo)
    end
end
