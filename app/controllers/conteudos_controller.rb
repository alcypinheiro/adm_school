class ConteudosController < ApplicationController
  before_action :set_conteudo, only: [:show, :edit, :update, :destroy]

  # GET /conteudos
  # GET /conteudos.json
  def index
    @conteudos = Conteudo.where(ativo: true)
  end

  # GET /conteudos/1
  # GET /conteudos/1.json
  def show
    @autor_conteudos = AutorConteudoConteudo.where(ativo: true, conteudo_id: params[:id])
  end

  # GET /conteudos/new
  def new
    @conteudo = Conteudo.new

    @autor_conteudo_conteudo = @conteudo.autor_conteudo_conteudos.build
  end

  # GET /conteudos/1/edit
  def edit
  end

  # POST /conteudos
  # POST /conteudos.json
  def create
    @conteudo_canal = Conteudo.where(ativo: true, canai_id: params[:conteudo][:canai_id])
    @conteudo = Conteudo.new(conteudo_params)
    @conteudo.image = params[:conteudo][:image]
    #@conteudo.video = params[:conteudo][:video]
    respond_to do |format|
    if @conteudo.save
    # @autor_conteudo_conteudo = AutorConteudoConteudo.new
    # @autor_conteudo_conteudo.conteudo_id = @conteudo.id
    # @autor_conteudo_conteudo.autor_conteudo_id = params[:conteudo][:pessoa_id]
        #format.html { redirect_to @conteudo, notice: 'Conteúdo cadastrado com sucesso.' }
        #format.json { render action: 'show', status: :created, location: @conteudo }
        format.html { redirect_to ver_conteudos_canal_canais_path(canai_id: params[:conteudo][:canai_id]) }
      else
        format.html { render action: 'new' }
        format.json { render json: @conteudo.errors, status: :unprocessable_entity }
        end
      end
  end

  # PATCH/PUT /conteudos/1
  # PATCH/PUT /conteudos/1.json
  def update
    respond_to do |format|
      if @conteudo.update(conteudo_params)
        @conteudo.update(image: params[:conteudo][:image])
        #@conteudo.update(video: params[:conteudo][:video])
        format.html { redirect_to @conteudo, notice: 'Conteúdo alterado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conteudos/1
  # DELETE /conteudos/1.json
  def destroy
    @conteudo.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to conteudos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conteudo
      @conteudo = Conteudo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conteudo_params
      params.require(:conteudo).permit(:conteudo_tipo_id, :nome, :canai_id, :pessoa_id, :descricao, :preco_sugerido, :ativo, autor_conteudo_conteudos_attributes: [:id, :autor_conteudo_id, :conteudo_id, :nome, :ativo, :_destroy])
    end
end
