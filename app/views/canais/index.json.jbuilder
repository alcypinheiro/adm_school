json.array!(@canais) do |canai|
  json.extract! canai, :id, :nome, :objetivo, :data_criacao, :criador, :empresa_id, :ativo
  json.url canai_url(canai, format: :json)
end
