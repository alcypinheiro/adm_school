json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nome, :email, :telefone, :cnpj, :cidade, :estado, :usuario_id, :bairro, :endereco, :complemento, :ativo
  json.url empresa_url(empresa, format: :json)
end
