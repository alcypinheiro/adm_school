json.array!(@modulos_pessoas) do |modulos_pessoa|
  json.extract! modulos_pessoa, :id, :pessoa_id, :modulo_id, :ativo, :permitido
  json.url modulos_pessoa_url(modulos_pessoa, format: :json)
end
