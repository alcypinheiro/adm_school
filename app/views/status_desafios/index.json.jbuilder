json.array!(@status_desafios) do |status_desafio|
  json.extract! status_desafio, :id, :nome, :sequencia, :ativo
  json.url status_desafio_url(status_desafio, format: :json)
end
