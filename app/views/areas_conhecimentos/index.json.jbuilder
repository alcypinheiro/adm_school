json.array!(@areas_conhecimentos) do |areas_conhecimento|
  json.extract! areas_conhecimento, :id, :nome, :ativo
  json.url areas_conhecimento_url(areas_conhecimento, format: :json)
end
