json.array!(@desafios_empresas) do |desafios_empresa|
  json.extract! desafios_empresa, :id, :ensino, :profissional, :solucao, :empresa_id, :areas_conhecimento_id, :contextualizacao, :principais_problemas, :dados_mercado, :termo_autorizacao, :ativo
  json.url desafios_empresa_url(desafios_empresa, format: :json)
end
