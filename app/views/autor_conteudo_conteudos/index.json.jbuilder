json.array!(@autor_conteudo_conteudos) do |autor_conteudo_conteudo|
  json.extract! autor_conteudo_conteudo, :id, :nome, :autor_conteudo_id, :conteudo_id, :ativo
  json.url autor_conteudo_conteudo_url(autor_conteudo_conteudo, format: :json)
end
