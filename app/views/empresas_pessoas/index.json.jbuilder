json.array!(@empresas_pessoas) do |empresas_pessoa|
  json.extract! empresas_pessoa, :id, :pessoa_id, :empresa_id, :ativo
  json.url empresas_pessoa_url(empresas_pessoa, format: :json)
end
