json.array!(@topicos_conteudos) do |topicos_conteudo|
  json.extract! topicos_conteudo, :id, :descricao, :objetivo, :resumo, :formato_conteudo_id, :ativo
  json.url topicos_conteudo_url(topicos_conteudo, format: :json)
end
