json.array!(@orientacoes_desafios) do |orientacoes_desafio|
  json.extract! orientacoes_desafio, :id, :desafios_empresa_id, :orientacao, :ativo
  json.url orientacoes_desafio_url(orientacoes_desafio, format: :json)
end
