json.array!(@modulos) do |modulo|
  json.extract! modulo, :id, :nome, :ativo
  json.url modulo_url(modulo, format: :json)
end
