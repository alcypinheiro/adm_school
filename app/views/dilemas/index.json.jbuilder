json.array!(@dilemas) do |dilema|
  json.extract! dilema, :id, :nome, :ativo
  json.url dilema_url(dilema, format: :json)
end
