json.array!(@orientacoes_empresas) do |orientacoes_empresa|
  json.extract! orientacoes_empresa, :id, :nome, :email, :texto
  json.url orientacoes_empresa_url(orientacoes_empresa, format: :json)
end
