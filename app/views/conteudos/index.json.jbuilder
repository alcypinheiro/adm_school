json.array!(@conteudos) do |conteudo|
  json.extract! conteudo, :id, :conteudo_tipo_id, :nome, :autor_conteudo, :preco_sugerido, :ativo
  json.url conteudo_url(conteudo, format: :json)
end
