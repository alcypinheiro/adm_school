json.array!(@empresarial_ramos) do |empresarial_ramo|
  json.extract! empresarial_ramo, :id, :nome, :ativo
  json.url empresarial_ramo_url(empresarial_ramo, format: :json)
end
