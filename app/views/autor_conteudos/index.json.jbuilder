json.array!(@autor_conteudos) do |autor_conteudo|
  json.extract! autor_conteudo, :id, :nome, :ativo
  json.url autor_conteudo_url(autor_conteudo, format: :json)
end
