json.array!(@formato_conteudos) do |formato_conteudo|
  json.extract! formato_conteudo, :id, :nome, :ativo
  json.url formato_conteudo_url(formato_conteudo, format: :json)
end
