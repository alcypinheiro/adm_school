json.array!(@metodologia_desafios) do |metodologia_desafio|
  json.extract! metodologia_desafio, :id, :nome, :ativo
  json.url metodologia_desafio_url(metodologia_desafio, format: :json)
end
